import React from "react";
import {Link, useLocation} from "react-router-dom";
import './Navbar.css'
// import mylogo from "../images/Buslogo.jpg"

function Navbar(){
    const location = useLocation();

    return(
        <nav className="navbar sticky-top  navbar-expand-lg navbar-dark bg-dark navbar-custom">
            <div className="container-fluid">
                {/* <img src={mylogo} className='logo' alt='Sevices'/>  */}
                <h2 style={{ color: 'white', fontStyle: 'italic', fontWeight: 'bold' }}>Easybus</h2>

                <button className="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                    <span className="navbar-toggler-icon"></span>
                </button>
                <div className="collapse navbar-collapse" id="navbarSupportedContent">
                    <ul className="navbar-nav ms-auto mb-2 mb-lg-0">
                        <li className="nav-item">
                            <Link to="/" className={location.pathname === "/" ? "nav-link active" : "nav-link"}>HOME</Link>
                        </li>
                        <li className="nav-item">
                            <Link to="/about" className={location.pathname === "/about" ? "nav-link active" : "nav-link"}>ABOUT US</Link>
                        </li>
                        <li className="nav-item">
                            <Link to="/bookticket" className={location.pathname === "/bookticket" ? "nav-link active" : "nav-link"}>BOOK-BUS</Link>
                        </li>
                        {/* <li className="nav-item">
                            <Link to="/parcel" className={location.pathname === "/parcel" ? "nav-link active" : "nav-link"}>Parcel</Link>
                        </li> */}
                        <li className="nav-item">
                            <Link to="/contact" className={location.pathname === "/contact" ? "nav-link active" : "nav-link"}>CONTACT US</Link>
                        </li>
                        <li className="nav-item">
                            <Link to="/admin" className={location.pathname === "/admin" ? "nav-link active" : "nav-link"}></Link>
                        </li>
                    </ul>
                </div>
            </div>
        </nav> 
    )
}

export default Navbar;
