import React from "react";
import {Link} from 'react-router-dom';

function Footer() {
    return(
        <>
        <section className="section footer  bg-dark text-white py-4 d-flex flex-column h-100">
            <div className="container">
                <div className="row">
                    <div className="col-md-4">
                        <h6>Bus Information</h6>
                        <hr/>
                        <p>
                            Bus are not available on Monday and friday
                        </p>
                    </div>
                    <div className="col-md-4">
                        <h6>Quick Link</h6>
                        <hr/>
                        <div>
                        <Link to="/" style={{ textDecoration: 'none' }}>Home</Link>
                        </div>
                        <div>
                        <Link to="/about" style={{ textDecoration: 'none' }}>About</Link>
                        </div>
                        <div>
                        <Link to="/contact" style={{ textDecoration: 'none' }}>Contact</Link>
                        </div>

                    </div>
                    <div className="col-md-4">
                        <h6>Contact information</h6>
                        <hr/>
                        <div><p className="="text-white mb-1>Thimphu,Bhutan</p></div>
                        <div><p className="="text-white mb-1>17780576</p></div>
                        <div><p className="="text-white mb-1>12200001.gcit@rub.edu.bt</p></div>
                    </div>
                </div>
            </div>
        </section>
       
        </>
    )
}
export default Footer;