import React, { useState, useEffect } from 'react';
import { useParams, Link } from 'react-router-dom';
import Footer from '../inc/footer';
import Navbar from '../inc/Navbar';

const BookedDetails = () => {
  const { busID, name, email, cid, seats } = useParams();

  const [userData, setUserData] = useState({ name, email, cid, seats });
  const [busData, setBusData] = useState({
    busName: '',
    date: '',
    time: '',
    from: '',
    to: '',
    price: '',
  });

  useEffect(() => {
    async function fetchData() {
      try {
        const res = await fetch(`http://localhost:8080/bus/${busID}`);

        if (res.ok) {
          const json = await res.json();

          setBusData({
            busName: json.busName,
            date: new Date(json.date).toDateString(),
            time: json.DepatureTime,
            from: json.from,
            to: json.to,
            price: json.price_per_seat,
          });
        }
      } catch (err) {
        alert(err.message);
      }
    }

    fetchData();
  }, []);

  return (
    <>
      <Navbar />
      <div className="mt-3 mb-5">
        <div className="container">
          <div className="card alert alert-info" role="alert">
            <div className="card-body p-3 bg-light">
              <h2 className="text-center">Bus booked successfully</h2>
              <h6 className="text-center">(Kindly check your email to receive a confirmation for your reservation.)</h6>
              <form className="w-75 mx-auto">
                <div className="form-group">
                  <label htmlFor="username">Passenger Name:</label>
                  <input type="text" className="form-control" id="username" name="username" value={userData.name} disabled />
                </div>
                <div className="form-group">
                  <label htmlFor="email">Email:</label>
                  <input type="email" className="form-control" id="email" name="email" value={userData.email} disabled />
                </div>
                <div className="form-group">
                  <label htmlFor="cid">CID (Customer ID):</label>
                  <input type="text" className="form-control" id="cid" name="cid" value={userData.cid} disabled />
                </div>
                <div className="form-group">
                  <label htmlFor="busname">Bus Name:</label>
                  <input type="text" className="form-control" id="busname" name="busname" value={busData.busName} disabled />
                </div>
                <div className="form-group">
                  <label htmlFor="departure-time">Departure Time:</label>
                  <input type="time" className="form-control" id="departure-time" name="departure-time" value={busData.time} disabled />
                </div>
                <div className="form-group">
                  <label htmlFor="from">From:</label>
                  <input type="text" className="form-control" id="from" name="from" value={busData.from} disabled />
                </div>
                <div className="form-group">
                  <label htmlFor="to">To:</label>
                  <input type="text" className="form-control" id="to" name="to" value={busData.to} disabled />
                </div>
                <div className="form-group">
                  <label htmlFor="date">Date:</label>
                  <input type="text" className="form-control" id="date" name="date" value={busData.date} disabled />
                </div>
                <div className="form-group">
                  <label htmlFor="price">Price:</label>
                  <input type="text" className="form-control" id="price" name="price" value={busData.price} disabled />
                </div>
                <div className="form-group">
                  <label htmlFor="seat-number">Seat Number:</label>
                  <input type="text" className="form-control" id="seat-number" name="seat-number" value={userData.seats} disabled />
                </div>
                <div className="d-flex justify-content-center">
                  <Link
                    to={{ pathname: `/seats/${busID}` }}
                    className="button-primary btn-sm"
                    style={{
                      textDecoration: 'none',
                      backgroundColor: 'blue',
                      color: 'white',
                      marginTop: '10px',
                      padding: '10px 30px',
                      marginBottom: '5px',
                      transition: 'background-color 0.3s',
                    }}
                    onMouseEnter={(e) => {
                      e.target.style.backgroundColor = 'turquoise';
                    }}
                    onMouseLeave={(e) => {
                      e.target.style.backgroundColor = 'blue';
                    }}
                  >
                    Proceed
                  </Link>
                </div>
              </form>
            </div>
          </div>
        </div>
      </div>
      <Footer />
    </>
  );
};

export default BookedDetails;

// import React, {useState, useEffect} from 'react'
// import { useParams, Link } from 'react-router-dom'
// import Footer from '../inc/footer';
// import Navbar from '../inc/Navbar';

// const BookedDetails = () => {
//   const { busID, name, email, cid, seats } = useParams();

//   const [userData, setUserData] = useState({name, email, cid, seats});
//   const [busData, setBusData] = useState({busName : "", date : "", time : "", from : "", to : "", price : ""});

//   useEffect(() => {
//     async function fetchData(){
//         try{
//           const res = await fetch(`http://localhost:8080/bus/${busID}`);
  
//           if (res.ok){
//             const json = await res.json();
  
//             setBusData({busName: json.busName, date: new Date(json.date).toDateString(), time: json.DepatureTime, from: json.from, to: json.to, price: json.price_per_seat})
//           }
//         }
//         catch (err){
//           alert(err.message);
//         }
//       }
  
//       fetchData();
//   }, []);

//   return (
//    <>
//    <Navbar/> 
//     <div className='mt-3'>
//       <div class="container">
//        <h2 className='text-center'>Bus booked successfully</h2>
//        <h6 className='text-center'>(Kindly check your email to receive a confirmation for your reservation.)</h6>
//         <form>
//             <div class="form-group">
//                 <label for="username">Passenger Name:</label>
//                 <input type="text" class="form-control" id="username" name="username" value={userData.name} disabled/>
//             </div>
//             <div class="form-group">
//                 <label for="email">Email:</label>
//                 <input type="email" class="form-control" id="email" name="email" value={userData.email}  disabled/>
//             </div>
//             <div class="form-group">
//                 <label for="cid">CID (Customer ID):</label>
//                 <input type="text" class="form-control" id="cid" name="cid" value={userData.cid}  disabled/>
//             </div>
//             <div class="form-group">
//                 <label for="busname">Bus Name:</label>
//                 <input type="text" class="form-control" id="busname" name="busname" value={busData.busName}  disabled/>
//             </div>
//             <div class="form-group">
//                 <label for="departure-time">Departure Time:</label>
//                 <input type="time" class="form-control" id="departure-time" name="departure-time" value={busData.time}  disabled/>
//             </div>
//             <div class="form-group">
//                 <label for="from">From:</label>
//                 <input type="text" class="form-control" id="from" name="from" value={busData.from}  disabled/>
//             </div>
//             <div class="form-group">
//                 <label for="to">To:</label>
//                 <input type="text" class="form-control" id="to" name="to" value={busData.to}  disabled/>
//             </div>
//             <div class="form-group">
//                 <label for="date">Date:</label>
//                 <input type="text" class="form-control" id="date" name="date" value={busData.date} disabled/>
//             </div>
//             <div class="form-group">
//                 <label for="price">Price:</label>
//                 <input type="number" class="form-control" id="price" name="price" value={busData.price} disabled/>
//             </div>
//             <div class="form-group">
//                 <label for="seat-number">Seat Number:</label>
//                 <input type="text" class="form-control" id="seat-number" name="seat-number" value={userData.seats} disabled/>
//             </div>
//             <div class="d-flex justify-content-center">
//             <Link
//                   to={{ pathname: `/seats/${busID}` }}
//                   className="button-primary btn-sm"
//                   style={{
//                     textDecoration: 'none',
//                     backgroundColor: 'blue',
//                     color: 'white',
//                     marginTop: '10px',
//                     padding: '10px 30px',
//                     marginBottom: '5px',
//                     transition: 'background-color 0.3s',
//                   }}
//                   onMouseEnter={(e) => {
//                     e.target.style.backgroundColor = 'turquoise';
//                   }}
//                   onMouseLeave={(e) => {
//                     e.target.style.backgroundColor = 'blue';
//                   }}
//                 >
//                   Proceed
//           </Link>

//           </div>
//         </form>
//       </div>
//     </div>
//     <Footer/>
//    </>
//   )
// }

// export default BookedDetails
