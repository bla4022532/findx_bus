import React, { useState, useEffect } from 'react';
import '../pages/Viewseat.css';
import { Link, useParams } from 'react-router-dom';
import Navbar from '../inc/Navbar';
import { Button, Modal } from 'react-bootstrap';

function Viewseat() {
  const [busData, setBusData] = useState([]);
  const [showModal, setShowModal] = useState(false);
  const { from, to, date } = useParams();

  useEffect(() => {
    let ignore = false;
    const fetchData = async () => {
      try {
        const response = await fetch('http://localhost:8080/create-bus', {
          method: 'GET',
          headers: {
            'Content-Type': 'application/json',
          },
        });
        const jsonData = await response.json();

        const newData = jsonData.filter(
          (data) =>
            from.toLowerCase() === data.from.toLowerCase() &&
            to.toLowerCase() === data.to.toLowerCase() &&
            date === data.date
        );
        if (newData.length === 0 && !ignore) {
          setShowModal(true);
        } else {
          setShowModal(false);
        }
        setBusData(newData);
      } catch (error) {
        console.error(error);
      }
    };

    fetchData();

    return () => {
      ignore = true;
    };
  }, []);

  const handleCloseModal = () => {
    setShowModal(false);
  };

  const formatTime = (time) => {
    const [hours, minutes] = time.split(':');
    let formattedTime = hours + ':' + minutes;

    const hour = parseInt(hours);
    if (hour < 12) {
      formattedTime += ' AM';
    } else {
      formattedTime += ' PM';
    }

    return formattedTime;
  };

  return (
    <>
      <Navbar />

      <div>
        <div className="container center-table">
          <div className="bus-info">
            <div className="table-responsive">
              <h3 className="text-center space-after-h3 mt-5" style={{ position: 'relative' }}>
                Available Bus
              </h3>
              <table className="table" id="t2">
                <thead>
                  <tr>
                    <th>Bus Name</th>
                    <th>Departure Time</th>
                    <th>Total seats</th>
                    <th>price_per_seat</th>
                    <th>Date</th>
                    <th>From</th>
                    <th>To</th>
                    <th>Make a choice</th>
                  </tr>
                </thead>
                <tbody className="main-table">
                  {busData.map((bus) => (
                    <tr key={bus.id}>
                      <td>{bus.busName}</td>
                      <td>{formatTime(bus.DepatureTime)}</td>
                      <td>{bus.totalseat}</td>
                      <td>{bus.price_per_seat}</td>
                      <td>{bus.date}</td>
                      <td>{bus.from}</td>
                      <td>{bus.to}</td>
                      <td>
                        <button className="view-seat-btn btn-sm">
                          <Link to={{ pathname: `/seats/${bus.id}` }} style={{ textDecoration: 'none' }}>
                            view seat
                          </Link>
                        </button>
                      </td>
                    </tr>
                  ))}
                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>

      <Modal show={showModal} onHide={handleCloseModal}>
        <Modal.Header closeButton>
          <Modal.Title>No Bus Found</Modal.Title>
        </Modal.Header>
        <Modal.Body>Sorry For inconvinence</Modal.Body>
        <Modal.Footer>
          <Button variant="secondary" onClick={handleCloseModal}>
            Close
          </Button>
        </Modal.Footer>
      </Modal>
    </>
  );
}

export default Viewseat;

// import React, { useState, useEffect } from 'react';
// import '../pages/Viewseat.css';
// import { Link, useParams } from 'react-router-dom';
// import Navbar from '../inc/Navbar';

// function Viewseat() {
//   const [busData, setBusData] = useState([]);
//   const { from, to, date } = useParams();

//   useEffect(() => {
//     let ignore = false;
//     const fetchData = async () => {
//       try {
//         const response = await fetch('http://localhost:8080/create-bus', {
//           method: 'GET',
//           headers: {
//             'Content-Type': 'application/json',
//           },
//         });
//         const jsonData =  await response.json();
  
//         const newData = jsonData.filter(data => 
//           from.toLowerCase() == data.from.toLowerCase() && 
//           to.toLowerCase() == data.to.toLowerCase() && 
//           date == data.date
//         );
//         if (newData.length === 0 && !ignore) alert("NO BUS FOUND!");
//         setBusData(newData);
//       } catch (error) {
//         console.error(error);
//       }
//     };

//     fetchData();

//     return () => {
//       ignore = true;
//     }
//   }, []);



  
//   return (
//     <>
//       <Navbar />
     
//       <div className='mt-5'>
//   <div className="container center-table mt-5">
//     <div className="bus-info">
//       <div className="table-responsive">
//       <h3 className="text-center space-after-h3 mt-5" style={{ position: 'relative' }}>
//       Available Bus
//     </h3>
//         <table className="table" id="t2">
//           <thead>
//             <tr>
//               <th>Bus Name</th>
//               <th>Departure Time</th>
//               <th>Total seats</th>
//               <th>price_per_seat</th>
//               <th>Date</th>
//               <th>From</th>
//               <th>To</th>
//               <th>Make a choice</th>
//             </tr>
//           </thead>
//           <tbody className="main-table">
//             {busData.map((bus) => (
//               <tr key={bus.id}>
//                 <td>{bus.busName}</td>
//                 <td>{bus.DepatureTime}</td>
//                 <td>{bus.totalseat}</td>
//                 <td>{bus.price_per_seat}</td>
//                 <td>{bus.date}</td>
//                 <td>{bus.from}</td>
//                 <td>{bus.to}</td>
//                 <td>
//                   <button className="view-seat-btn btn-sm">
//                     <Link to={{ pathname: `/seats/${bus.id}` }} style={{ textDecoration: 'none' }}>
//                       view seat
//                     </Link>
//                   </button>
//                 </td>
//               </tr>
//             ))}
//           </tbody>
//         </table>
//       </div>
//     </div>
//   </div>
// </div>

//       {/* <Footer /> */}
//     </>
//   );
// }

// export default Viewseat;


