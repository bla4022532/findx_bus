import React, { useState } from "react";
import { Button, Modal } from "react-bootstrap";
import { useNavigate } from 'react-router-dom';
import "../pages/Contact.css";
import Navbar from "../inc/Navbar";
import Footer from "../inc/footer";

function Contact() {
  const [showModal, setShowModal] = useState(false);
  const [username, setUsername] = useState("");
  const [phonenumber, setPhoneNumber] = useState("");
  const [email, setEmail] = useState("");
  const [feedback, setFeedback] = useState("");
  const [error, setError] = useState("");
  const navigate = useNavigate();

  const handleClose = () => setShowModal(false);

  const handleSend = async () => {
    if (!username || !phonenumber || !email || !feedback) {
      setError("All fields are required");
      setTimeout(() => setError(""), 5000);//clear error after 5 seconds
      return;
    }

    // Phone number validation
    const phoneRegex = /^(17|77)\d{6}$/;
    if (!phoneRegex.test(phonenumber)) {
      setError("Invalid phone number. Phone number should start with 17 or 77 and have 8 digits.");
      setTimeout(() => setError(""), 7000); // Clear error after 7 seconds
      return;
    }

    // Email validation
    const emailRegex = /^[A-Za-z0-9._%+-]+@(gmail\.com|rub\.edu\.bt)$/;
  if (!emailRegex.test(email)) {
    setError("Invalid email address. Email should be in the format example@gmail.com or example@rub.edu.bt.");
    setTimeout(() => setError(""), 7000); // Clear error after 7 seconds
    return;
  }

    // Code to send the form data goes here

    try {
      const formData = {
        username,
        phonenumber,
        email,
        feedback,
      };

      const response = await fetch('http://localhost:8080/feedback', {
        method: 'POST',
        headers: {
          'Content-Type': 'application/json'
        },
        body: JSON.stringify(formData)
      });

      if (response.ok) {
        // Feedback sent successfully
        setShowModal(true);
      } else {
        // Handle error
        const errorData = await response.json();
        console.error('Failed to send feedback:', errorData.message);
      }

    } catch (error) {
      console.error('An error occurred:', error);
    }
  };

  return (
    <>
      <Navbar />
      <div>
        <section className="contact-section mb-5 mt-1">
          <div className="container">
            <div className="card shadow ">
              <div className="card-body">
                {error && <p className="text-danger">{error}</p>} {/* Display error message here */}
                <div className="row">
                  <div className="col-md-6">
                    <h5>Contact form</h5>
                    <hr />
                    <div className="form-group">
                      <label className="mb-1">Full Name</label>
                      <input
                        type="text"
                        className="form-control"
                        placeholder="Enter your name"
                        value={username}
                        onChange={(e) => setUsername(e.target.value)}
                      ></input>
                    </div>
                    <div className="form-group">
                      <label className="mb-1">Phone number</label>
                      <input
                        type="number"
                        className="form-control"
                        placeholder="Enter number"
                        value={phonenumber}
                        onChange={(e) => setPhoneNumber(e.target.value)}
                      ></input>
                    </div>
                    <div className="form-group">
                      <label className="mb-1">Email Address</label>
                      <input
                        type="email"
                        className="form-control"
                        placeholder="Enter email"
                        required
                        value={email}
                        onChange={(e) => setEmail(e.target.value)}
                      ></input>
                    </div>
                    <div className="form-group">
                      <label className="mb-1">Feedback</label>
                      <textarea
                        rows="3"
                        className="form-control"
                        placeholder="Type your message..."
                        required
                        value={feedback}
                        onChange={(e) => setFeedback(e.target.value)}
                      ></textarea>
                    </div>
                    <div className="form-group py-3">
                      <button
                        type="button"
                        className="btn btn-primary shadow d-block mx-auto"
                        style={{ width: '35%' }}
                        onClick={handleSend}
                      >
                        Send
                      </button>
                    </div>
                  </div>
                  <div className="col-md-6">
                    <h2>Sites Information</h2>
                    <p>Call Center: 123</p>
                    <p>Location: Thimphu</p>
                    <div className="map-container">
                      <iframe
                        title="Google Map"
                        width="100%"
                        height="450"
                        frameBorder="0"
                        src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3540.04537132033!2d89.63809197792905!3d27.467846815265045!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x39e19412bdb607e3%3A0x3011c9cc643c01a0!2sThimphu%20Bus%20Station!5e0!3m2!1sen!2sbt!4v1685033614751!5m2!1sen!2sbt"
                        allowFullScreen
                        loading="lazy"
                        referrerpolicy="no-referrer-when-downgrade"
                      ></iframe>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </section>
        <Modal show={showModal} onHide={handleClose}>
          <Modal.Header closeButton>
            <Modal.Title>Thank you for your message</Modal.Title>
          </Modal.Header>
          <Modal.Body>Your message has been sent successfully.</Modal.Body>
          <Modal.Footer>
            <Button variant="secondary" onClick={() => { setShowModal(false); navigate('/'); }}>
              Close
            </Button>
          </Modal.Footer>
        </Modal>
      </div>
      <Footer />
    </>
  );
}

export default Contact;


// import React, { useState } from "react";
// import { Button, Modal } from "react-bootstrap";
// import { useNavigate } from 'react-router-dom';
// import "../pages/Contact.css";
// import Navbar from "../inc/Navbar";
// import Footer from "../inc/footer";

// function Contact() {
//   const [showModal, setShowModal] = useState(false);
//   const [username, setUsername] = useState("");
//   const [phonenumber, setPhoneNumber] = useState("");
//   const [email, setEmail] = useState("");
//   const [feedback, setFeedback] = useState("");
//   const [error, setError] = useState("");
//   const navigate = useNavigate();

//   const handleClose = () => setShowModal(false);

//   const handleSend = async () => {
//     if (!username || !phonenumber || !email || !feedback) {
//       setError("All fields are required");
//       setTimeout(() => setError(""), 5000);//clear error after 5 seconds
//       return;
//     }

//     // Phone number validation
//     const phoneRegex = /^(17|77)\d{6}$/;
//     if (!phoneRegex.test(phonenumber)) {
//       setError("Invalid phone number. Phone number should start with 17 or 77 and have 8 digits.");
//       setTimeout(() => setError(""), 5000); // Clear error after 5 seconds
//       return;
//     }

//     // Code to send the form data goes here

//     try {
//       const formData = {
//         username,
//         phonenumber,
//         email,
//         feedback,
//       };

//       const response = await fetch('http://localhost:8080/feedback', {
//         method: 'POST',
//         headers: {
//           'Content-Type': 'application/json'
//         },
//         body: JSON.stringify(formData)
//       });

//       if (response.ok) {
//         // Feedback sent successfully
//         setShowModal(true);
//       } else {
//         // Handle error
//         const errorData = await response.json();
//         console.error('Failed to send feedback:', errorData.message);
//       }

//     } catch (error) {
//       console.error('An error occurred:', error);
//     }
//   };

//   return (
//     <>
//       <Navbar />
//       <div>
//         <section className="contact-section mb-3 mt-5">
//           <div className="container">
//             <div className="card shadow ">
//               <div className="card-body">
//                 <div className="row">
//                   <div className="col-md-6">
//                     <h5>Contact form</h5>
//                     <hr />
//                     <div className="form-group">
//                       <label className="mb-1">Full Name</label>
//                       <input
//                         type="text"
//                         className="form-control"
//                         placeholder="Enter your name"
//                         value={username}
//                         onChange={(e) => setUsername(e.target.value)}
//                       ></input>
//                     </div>
//                     <div className="form-group">
//                       <label className="mb-1">Phone number</label>
//                       <input
//                         type="number"
//                         className="form-control"
//                         placeholder="Enter number"
//                         value={phonenumber}
//                         onChange={(e) => setPhoneNumber(e.target.value)}
//                       ></input>
//                     </div>
//                     <div className="form-group">
//                       <label className="mb-1">Email Address</label>
//                       <input
//                         type="email"
//                         className="form-control"
//                         placeholder="Enter email"
//                         required
//                         value={email}
//                         onChange={(e) => setEmail(e.target.value)}
//                       ></input>
//                     </div>
//                     <div className="form-group">
//                       <label className="mb-1">Feedback</label>
//                       <textarea
//                         rows="3"
//                         className="form-control"
//                         placeholder="Type your message..."
//                         required
//                         value={feedback}
//                         onChange={(e) => setFeedback(e.target.value)}
//                       ></textarea>
//                     </div>
//                     <div className="form-group py-3">
//                       <button
//                         type="button"
//                         className="btn btn-primary shadow d-block mx-auto"
//                         style={{ width: '35%' }}
//                         onClick={handleSend}
//                       >
//                         Send
//                       </button>
//                     </div>
//                     {error && <p className="text-danger">{error}</p>}
//                   </div>
//                   <div className="col-md-6">
//                     <h2>Sites Information</h2>
//                     <p>Call Center: 123</p>
//                     <p>Location: Thimphu</p>
//                     <div className="map-container">
//                       <iframe
//                         title="Google Map"
//                         width="100%"
//                         height="450"
//                         frameBorder="0"
//                         src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3540.04537132033!2d89.63809197792905!3d27.467846815265045!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x39e19412bdb607e3%3A0x3011c9cc643c01a0!2sThimphu%20Bus%20Station!5e0!3m2!1sen!2sbt!4v1685033614751!5m2!1sen!2sbt"
//                         allowFullScreen
//                         loading="lazy"
//                         referrerpolicy="no-referrer-when-downgrade"
//                       ></iframe>
//                     </div>
//                   </div>
//                 </div>
//               </div>
//             </div>
//           </div>
//         </section>
//         <Modal show={showModal} onHide={handleClose}>
//           <Modal.Header closeButton>
//             <Modal.Title>Thank you for your message</Modal.Title>
//           </Modal.Header>
//           <Modal.Body>Your message has been sent successfully.</Modal.Body>
//           <Modal.Footer>
//             <Button variant="secondary" onClick={() => { setShowModal(false); navigate('/'); }}>
//               Close
//             </Button>
//           </Modal.Footer>
//         </Modal>
//       </div>
//       <Footer />
//     </>
//   );
// }

// export default Contact;




