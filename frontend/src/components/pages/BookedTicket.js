import React, { useState } from 'react';
import { Link } from 'react-router-dom';
import { Container, Row, Col, Form, Modal, Button } from 'react-bootstrap';
import axios from 'axios';
import '../../components/BookTicket.css';
import Footer from '../inc/footer';
import Navbar from '../inc/Navbar';

const BookedTicket = () => {
  const [from, setFrom] = useState('');
  const [to, setTo] = useState('');
  const [date, setDate] = useState('');
  const [showModal, setShowModal] = useState(false);

  const searchBus = async () => {
    if (!from || !to || !date) {
      setShowModal(true);
      return;
    }

    try {
      const response = await axios.post('/search-bus', {
        from,
        to,
        date,
      });
      const availableBuses = response.data;
      // Handle the bus availability response here
      console.log('Available Buses:', availableBuses);
    } catch (error) {
      console.error('Error fetching bus availability:', error);
    }
  };

  const handleCloseModal = () => {
    setShowModal(false);
  };

  return (
    <>
      <Navbar />
      <div className="mt-auto mb-5">
        <div className="busimg  d-none d-md-flex d-xl-flex flex-column align-items-center">
          <Container className="fluid mb-3">
            <Row>
              <Col lg={2}></Col>
              <Col lg={8} className=" jerry bg-white p-4 rounded-1 mt-5">
                <div className="d-flex flex-column">
                  <div className="d-flex align-items-center text-white fw-bold">
                    <Form.Check
                      className="text-dark justify-content-between"
                      inline
                      label="One Way"
                      name="flexRadioDisabled"
                      id="flexRadioDisabled"
                      defaultChecked
                      type="radio"
                    />
                  </div>
                  {/* input field */}
                  <div className="d-flex align-items-center py-2 justify-content-between">
                    <div>
                      <Form.Label
                        htmlFor="exampleDataList"
                        className="label-from mb-2  text-dark fs-8 fw-bold ml-2"
                      >
                        From
                      </Form.Label>
                      <Form.Control
                        list="datalistOptions"
                        id="exampleDataList"
                        placeholder="Place"
                        className="form-control"
                        value={from}
                        onChange={(e) => setFrom(e.target.value)}
                      />
                      <datalist id="datalistOptions">
                        <option value="Thimphu" />
                        <option value="Paro" />
                        <option value="Punakha" />
                        <option value="Trashigang" />
                        <option value="Gelephu" />
                        <option value="Bumthang" />
                        <option value="Mongar" />
                        <option value="Samtse" />
                        <option value="Pemagatshel" />
                        <option value="Chukha" />
                        <option value="Dagana" />
                        <option value="Gasa" />
                        <option value="Haa" />
                        <option value="Lhuentse" />
                        <option value="Sarpang" />
                        <option value="Wangdue" />
                        <option value="Trongsa" />
                        <option value="Zhemgang" />
                        <option value="Tsirang" />
                        <option value="Trashiyangtse" />
                      </datalist>
                    </div>
                    {/* From end */}

                    {/* To */}
                    <div>
                      <Form.Label
                        htmlFor="exampleDataList"
                        className="mb-2 text-dark fs-8 fw-bold ml-2"
                      >
                        To
                      </Form.Label>
                      <Form.Control
                        list="datalistOptions"
                        id="exampleDataList"
                        placeholder="Place"
                        className="form-control"
                        value={to}
                        onChange={(e) => setTo(e.target.value)}
                      />

                      <datalist id="datalistOptions">
                        <option value="Thimphu" />
                        <option value="Paro" />
                        <option value="Punakha" />
                        <option value="Trashigang" />
                        <option value="Gelephu" />
                        <option value="Bumthang" />
                        <option value="Mongar" />
                        <option value="Samtse" />
                        <option value="Pemagatshel" />
                        <option value="Chukha" />
                        <option value="Dagana" />
                        <option value="Gasa" />
                        <option value="Haa" />
                        <option value="Lhuentse" />
                        <option value="Sarpang" />
                        <option value="Wangdue" />
                        <option value="Trongsa" />
                        <option value="Zhemgang" />
                        <option value="Tsirang" />
                        <option value="Trashiyangtse" />
                      </datalist>
                    </div>
                    {/* TO end */}

                    {/* Departure */}
                    <div>
                      <Form.Label
                        htmlFor=""
                        className="mb-2 text-dark fs-8 fw-bold"
                      >
                        Date
                      </Form.Label>
                      <Form.Control
                        type="date"
                        className="form-control"
                        min={new Date().toISOString().split('T')[0]}  // Updated code to set the minimum date to the current date
                        value={date}
                        onChange={(e) => setDate(e.target.value)}
                      />
                    </div>
                  </div>
                  <div className="text-center mt-3">
                    <Link
                      onClick={searchBus}
                      to={{
                        pathname: !from || !to || !date ? '' : `/viewseat/${from}/${to}/${date}`,
                      }}
                      className="buton-primary btn-sm"
                    >
                      Search Bus
                    </Link>
                  </div>
                </div>
              </Col>
            </Row>
          </Container>
        </div>
      </div>
      <div className="container">
        <h4 className="hello text-center">Why Online booking?</h4>
        <div className="row justify-content-center">
          <div className="col-sm-3 my-cell">
            <h3 className="trans">Safety</h3>
            <p>
              Your safety is our highest priority. Our Buses are regularly Serviced to ensure your safety.
              Moreover, our Bus Drivers are highly Professional just so you can reach your destination Safely.
            </p>
          </div>
          <div className="col-sm-3 my-cell">
            <h3 className="trans">Comfort</h3>
            <p>
              With our Fleet of Coaster, comfort on bumpy long drives is ensured. Our vehicles are meticulously
              designed to provide a smooth and enjoyable experience, even on the most challenging roads.
            </p>
          </div>
          <div className="col-sm-3 my-cell">
            <h3 className="trans">Reliability</h3>
            <p>
              Reliability is at the core of our bus service. We understand the importance of punctuality and strive
              to ensure that our buses operate on schedule, providing passengers with a dependable transportation.
            </p>
          </div>
        </div>
      </div>

      {/* Modal */}
      <Modal show={showModal} onHide={handleCloseModal}>
        <Modal.Header closeButton>
          <Modal.Title>All fields are required</Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <p>Please fill in all the fields before searching for a bus.</p>
        </Modal.Body>
        <Modal.Footer>
          <Button variant="secondary" onClick={handleCloseModal}>
            Close
          </Button>
        </Modal.Footer>
      </Modal>

      <Footer />
    </>
  );
};

export default BookedTicket;


// import React, { useState } from 'react';
// import { Link } from 'react-router-dom';
// import { Container, Row, Col, Form } from 'react-bootstrap';
// import axios from 'axios';
// import '../../components/BookTicket.css';
// import Footer from '../inc/footer';
// import Navbar from '../inc/Navbar';

// const BookedTicket = () => {
//   const [from, setFrom] = useState('');
//   const [to, setTo] = useState('');
//   const [date, setDate] = useState('');

//   const searchBus = async () => {
//     try {
//       const response = await axios.post('/search-bus', {
//         from,
//         to,
//         date,
//       });
//       const availableBuses = response.data;
//       // Handle the bus availability response here
//       console.log('Available Buses:', availableBuses);
//     } catch (error) {
//       console.error('Error fetching bus availability:', error);
//     }
//   };

//   return (
//     <>
//     <Navbar/>
//       <div className="mt-auto mb-5">
//         <div className="busimg  d-none d-md-flex d-xl-flex flex-column align-items-center">
//           <Container className="fluid mb-3">
//             <Row>
//               <Col lg={2}></Col>
//               <Col lg={8} className=" jerry bg-white p-4 rounded-1 mt-5">
//                 <div className="d-flex flex-column">
//                   <div className="d-flex align-items-center text-white fw-bold">
//                     <Form.Check
//                       className="text-dark justify-content-between"
//                       inline
//                       label="One Way"
//                       name="flexRadioDisabled"
//                       id="flexRadioDisabled"
//                       defaultChecked
//                       type="radio"
//                     />
//                   </div>
//                   {/* input field */}
//                   <div className="d-flex align-items-center py-2 justify-content-between">
//                     <div>
//                       <Form.Label
//                         htmlFor="exampleDataList"
//                         className="label-from mb-2  text-dark fs-8 fw-bold ml-2"
//                       >
//                         From
//                       </Form.Label>
//                       <Form.Control
//                           list="datalistOptions"
//                           id="exampleDataList"
//                           placeholder="Place"
//                           className="form-control"
//                           value={from}
//                           onChange={(e) => setFrom(e.target.value)}
//                         />
//                       <datalist id="datalistOptions">
//                         <option value="Thimphu" />
//                         <option value="Paro" />
//                         <option value="Punakha" />
//                         <option value="Trashigang" />
//                         <option value="Gelephu" />
//                         <option value="Bumthang" />
//                         <option value="Mongar" />
//                         <option value="Samtse" />
//                         <option value="Pemagatshel" />
//                         <option value="Chukha" />
//                         <option value="Dagana" />
//                         <option value="Gasa" />
//                         <option value="Haa" />
//                         <option value="Lhuentse" />
//                         <option value="Sarpang" />
//                         <option value="Wangdue" />
//                         <option value="Trongsa" />
//                         <option value="Zhemgang" />
//                         <option value="Tsirang" />
//                         <option value="Trashiyangtse" />
//                       </datalist>
//                     </div>
//                     {/* From end */}

//                     {/* To */}
//                     <div>
//                       <Form.Label
//                         htmlFor="exampleDataList"
//                         className="mb-2 text-dark fs-8 fw-bold ml-2"
//                       >
//                         To
//                       </Form.Label>
//                       <Form.Control
//                           list="datalistOptions"
//                           id="exampleDataList"
//                           placeholder="Place"
//                           className="form-control"
//                           value={to}
//                           onChange={(e) => setTo(e.target.value)}
//                         />

//                       <datalist id="datalistOptions">
//                       <option value="Thimphu" />
//                         <option value="Paro" />
//                         <option value="Punakha" />
//                         <option value="Trashigang" />
//                         <option value="Gelephu" />
//                         <option value="Bumthang" />
//                         <option value="Mongar" />
//                         <option value="Samtse" />
//                         <option value="Pemagatshel" />
//                         <option value="Chukha" />
//                         <option value="Dagana" />
//                         <option value="Gasa" />
//                         <option value="Haa" />
//                         <option value="Lhuentse" />
//                         <option value="Sarpang" />
//                         <option value="Wangdue" />
//                         <option value="Trongsa" />
//                         <option value="Zhemgang" />
//                         <option value="Tsirang" />
//                         <option value="Trashiyangtse" />
//                       </datalist>
//                     </div>
//                     {/* TO end */}

//                     {/* Departure */}
//                     <div>
//                       <Form.Label
//                         htmlFor=""
//                         className="mb-2 text-dark fs-8 fw-bold"
//                       >
//                         Date
//                       </Form.Label>
//                       <Form.Control
//                           type="date"
//                           className="form-control"
//                           value={date}
//                           onChange={(e) => setDate(e.target.value)}
//                         />
//                     </div>
//                   </div>
//                   <div className="text-center mt-3">
//                   <Link
//                     onClick={() => (!from || !to || !date) && alert("All fields are required!")}
//                     to={{pathname: !from || !to || !date ? "" : `/viewseat/${from}/${to}/${date}`}} 
//                     className="buton-primary btn-sm"
//                   >
//                     Search Bus
//                   </Link>
//                   </div>
                 
//                 </div>
//               </Col>
//             </Row>
//           </Container>
//         </div>
//       </div>
//         <div class="container">
//         <h4 class=" hello text-center">Why Online booking?</h4>
//         <div class="row justify-content-center">
//           <div class="col-sm-3 my-cell">
//           <h3 className="trans">Safety</h3>
//           <p>Your safety is our highest priority. Our Buses are regularly Serviced to ensure your safety. Moreover, our Bus Drivers are highly Professional just so you can reach your destination Safely.</p>
//         </div>
//         <div class="col-sm-3 my-cell">
//           <h3 className="trans">Comfort</h3>
//           <p>With our Fleet of Coaster, comfort on bumpy long drives is ensured. Our vehicles are meticulously designed to provide a smooth and enjoyable experience, even on the most challenging roads.</p>
//         </div>
//         <div class="col-sm-3 my-cell">
//           <h3 className="trans">Reliability</h3>
//           <p>Reliability is at the core of our bus service. We understand the importance of punctuality and strive to ensure that our buses operate on schedule, providing passengers with a dependable transportation.</p>
//         </div>
//   </div>
// </div>

//       <Footer/>
//     </>
//   );
// };

// export default BookedTicket;
