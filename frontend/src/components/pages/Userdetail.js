import React, { useState } from 'react';
import Navbar from '../inc/Navbar';
import Footer from '../inc/footer';
import scan from "../images/qr.jpg";
import { useParams, useNavigate } from 'react-router-dom';
import Popup from '../inc/popup'; 

const Userdetail = () => {
  const navigate = useNavigate();
  const { busID, seats } = useParams();
  const [data, setData] = useState({ name: '', email: '', cid: '' });
  const [showPopup, setShowPopup] = useState(false);
  const [error, setError] = useState('');

  const handleSubmit = async (e) => {
    e.preventDefault();

    if (!data.name || !data.email || !data.cid || data.cid.length !== 11) {
      setError('Please enter all fields correctly.');
      setTimeout(() => {
        setError('');
      }, 5000);
      return;
    }

    try {
      const response = await fetch(`http://localhost:8080/manage-ride/${busID}`, {
        method: 'POST',
        headers: {
          'Content-Type': 'application/json'
        },
        body: JSON.stringify({ ...data, seats })
      });

      if (response.ok) {
        setShowPopup(true);
        navigate(`/bookeddetails/${busID}/${data.name}/${data.email}/${data.cid}/${seats}`);
      } else {
        const err = await response.text();
        setError(err);
        setTimeout(() => {
          setError('');
        }, 5000);
      }
    } catch (err) {
      setError(err.message);
      setTimeout(() => {
        setError('');
      }, 5000);
    }
  };

  const closePopup = () => {
    setShowPopup(false);
  };

  const handleEmailChange = (e) => {
    const enteredEmail = e.target.value.toLowerCase();
    const emailRegex = /^[a-z0-9._%+-]+@(gmail\.com|rub\.edu\.bt)$/;

    if (!emailRegex.test(enteredEmail)) {
      setError('Please enter a valid email address.');
      setTimeout(() => {
        setError('');
      }, 5000);
    } else {
      const domain = enteredEmail.split('@')[1];
      if (
        (domain === 'gmail.com' && enteredEmail.endsWith('@gmail.com')) ||
        (domain === 'rub.edu.bt' && enteredEmail.endsWith('@rub.edu.bt'))
      ) {
        setError('');
      } else {
        setError('Please enter a valid email address with the correct domain extension.');
        setTimeout(() => {
          setError('');
        }, 5000);
      }
    }

    setData({ ...data, email: enteredEmail });
  };

  const handleCidChange = (e) => {
    const enteredCid = e.target.value;

    if (enteredCid.length !== 11) {
      setError('Please enter a valid CID of exactly 11 digits.');
      setTimeout(() => {
        setError('');
      }, 5000);
    } else {
      setError('');
    }

    setData({ ...data, cid: enteredCid });
  };

  return (
    <>
      <Navbar />
      <div className="content mb-5 mt-5">
        <div className="container">
          <div className="card">
            <div className="card-body">
              <div className="row">
                <div className="col-md-7">
                  <h5 className="card-title">Passenger Details</h5>
                  {error && <p style={{ color: 'red' }}>{error}</p>}
                  <form onSubmit={handleSubmit}>
                    <div className="form-group form-container">
                      <label htmlFor="name">Name:</label>
                      <input
                        type="text"
                        className="form-control"
                        id="name"
                        placeholder="Enter your name"
                        onChange={(e) => { setData({ ...data, name: e.target.value }) }}
                      />
                    </div>
                    <div className="form-group form-container">
                      <label htmlFor="email">Email:</label>
                      <input
                        type="email"
                        className="form-control"
                        id="email"
                        placeholder="Enter your email"
                        onChange={handleEmailChange}
                      />
                    </div>
                    <div className="form-group form-container">
                      <label htmlFor="cid">CID:</label>
                      <input
                        type="text"
                        className="form-control"
                        id="cid"
                        placeholder="Enter your CID"
                        onChange={handleCidChange}
                      />
                    </div>
                    <div className="text-center">
                      <button type="submit" className="btn btn-primary">
                        Submit
                      </button>
                    </div>
                  </form>
                </div>

                <div className="col-md-5 d-flex">
                  <img src={scan} className="w-50 border-bottom" alt="members" style={{ marginRight: '500px' }} />
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div className="container mt-5 text-center">
        <div className="alert alert-info" role="alert">
          <h5 className="alert-heading">Important Payment Instructions</h5>
          <p>To secure your bus seat reservation, please use the "Scan and Pay" feature in our bank's through scanning QR code.</p>
          <p>If you skip this step, your booking will not be finalized, and your seat may be given to another customer.</p>
          <p>For any issues or need any assistance, contact us for immediate assistance.</p>
        </div>
      </div>
      <Footer />
      {showPopup && <Popup message="Submitted successfully!" onClose={closePopup} />}
      <style>
        {`
          .popup {
            position: fixed;
            top: 50%;
            left: 50%;
            transform: translate(-50%, -50%);
            background-color: white;
            padding: 20px;
            border: 1px solid black;
            border-radius: 4px;
            box-shadow: 0 2px 4px rgba(0, 0, 0, 0.2);
          }
        `}
      </style>
    </>
  );
};

export default Userdetail;



// import React, { useState } from 'react';
// import Navbar from '../inc/Navbar';
// import Footer from '../inc/footer';
// import scan from "../images/qr.jpg";
// import { useParams, useNavigate } from 'react-router-dom';
// import Popup from '../inc/popup'; 

// const Userdetail = () => {
//   const navigate = useNavigate();
//   const { busID, seats } = useParams();
//   const [data, setData] = useState({ name: '', email: '', cid: null });
//   const [showPopup, setShowPopup] = useState(false);
//   const [error, setError] = useState('');

//   const handleSubmit = async (e) => {
//     e.preventDefault();

//     if (!data.name || !data.email || !data.cid) {
//       setError('All fields are required.');
//       return;
//     }

//     try {
//       const response = await fetch(`http://localhost:8080/manage-ride/${busID}`, {
//         method: 'POST',
//         headers: {
//           'Content-Type': 'application/json'
//         },
//         body: JSON.stringify({ ...data, seats })
//       });

//       if (response.ok) {
//         setShowPopup(true);
//         navigate(`/bookeddetails/${busID}/${data.name}/${data.email}/${data.cid}/${seats}`)
//       } else {
//         const err = await response.text()
//         alert(err)
//       }
//     } catch (err) {
//       alert(err.message);
//     }
//   };

//   const closePopup = () => {
//     setShowPopup(false);
//   };

//   const handleEmailChange = (e) => {
//     const enteredEmail = e.target.value;
//     const emailRegex = /^[A-Za-z0-9._%+-]+@(rub\.edu\.bt|gmail\.com)$/;

//     if (!emailRegex.test(enteredEmail)) {
//       setError('Please enter a valid email address.');
//     } else {
//       setError('');
//     }

//     setData({ ...data, email: enteredEmail });
//   };

//   return (
//     <>
//       <Navbar />
//       <div className="content mb-5 mt-5">
//         <div className="container">
//           <div className="card">
//             <div className="card-body">
//               <div className="row">
//                 <div className="col-md-7">
//                   <h5 className="card-title">Passenger Details</h5>
//                   {error && <p style={{ color: 'red' }}>{error}</p>}
//                   <form onSubmit={handleSubmit}>
//                     <div className="form-group form-container">
//                       <label htmlFor="name">Name:</label>
//                       <input
//                         type="text"
//                         className="form-control"
//                         id="name"
//                         placeholder="Enter your name"
//                         onChange={(e) => { setData({ ...data, name: e.target.value }) }}
//                       />
//                     </div>
//                     <div className="form-group form-container">
//                       <label htmlFor="email">Email:</label>
//                       <input
//                         type="email"
//                         className="form-control"
//                         id="email"
//                         placeholder="Enter your email"
//                         onChange={handleEmailChange}
//                       />
//                     </div>
//                     <div className="form-group form-container">
//                       <label htmlFor="cid">CID:</label>
//                       <input
//                         type="number"
//                         className="form-control"
//                         id="cid"
//                         placeholder="Enter your CID"
//                         onChange={(e) => { setData({ ...data, cid: e.target.value }) }}
//                       />
//                     </div>
//                     <div className="text-center">
//                       <button type="submit" className="btn btn-primary">
//                         Submit
//                       </button>
//                     </div>
//                   </form>
//                 </div>

//                 <div className="col-md-5 d-flex">
//                 <img
//                   src={scan}
//                   className="w-50 border-bottom"
//                   alt="members"
//                   style={{ marginRight: '500px' }}
//                 />
//               </div>

//               </div>
//             </div>
//           </div>
//         </div>
//       </div>
//       <div class="container mt-5 text-center">
//   <div class="alert alert-info" role="alert">
//     <h5 class="alert-heading">Important Payment Instructions</h5>
//     <p>To secure your bus seat reservation, please use the "Scan and Pay" feature in our bank's mobile app.</p>
//     <p>If you skip this step, your booking will not be finalized, and your seat may be given to another customer.</p>
//     <p>For any issues or need any assistance, contact us for immediate assistance.</p>
//   </div>
// </div>


//       <Footer />
//       {showPopup && (
//         <Popup message="Submitted successfully!" onClose={closePopup} />
//       )}
//       <style>
//         {`
//           .popup {
//             position: fixed;
//             top: 50%;
//             left: 50%;
//             transform: translate(-50%, -50%);
//             background-color: white;
//             padding: 20px;
//             border: 1px solid black;
//             border-radius: 4px;
//             box-shadow: 0 2px 4px rgba(0, 0, 0, 0.2);
//           }
//         `}
//       </style>
//     </>
//   );
// };

// export default Userdetail;


