import React from "react";
import Slider from "../inc/Slider";
import profile1 from "../images/nima.jpg";
import profile2 from "../images/kd.jpg";
import profile3 from "../images/sonam.jpg";
import profile4 from "../images/sam.jpg";
import Navbar from "../inc/Navbar";
import Footer from "../inc/footer";

function Home() {
  return (
    <>
      <Navbar />
      <div>
        <Slider />
        <section className="section mb-5 mt-4 border-top">
          <div className="container">
            <div className="row">
              <div className="col-md-12 mb-4 text-center">
                <h3 className="main-heading">Staff</h3>
                <div className="underline mx-auto"></div>
              </div>
              <div className="col-md-3">
                <div className="card shadow">
                  <img
                    src={profile1}
                    className="w-100 border-bottom"
                    alt="members"
                  />
                  <div className="card-body text-center">
                    <h6>Manager</h6>
                    <div className="underline"></div>
                  </div>
                </div>
              </div>
              <div className="col-md-3">
                <div className="card shadow">
                  <img
                    src={profile2}
                    className="w-100 border-bottom"
                    alt="members"
                  />
                  <div className="card-body text-center">
                    <h6>Bus conductor</h6>
                    <div className="underline"></div>
                  </div>
                </div>
              </div>
              <div className="col-md-3">
                <div className="card shadow">
                  <img
                    src={profile3}
                    className="w-100 border-bottom"
                    alt="members"
                  />
                  <div className="card-body text-center">
                    <h6>Counter</h6>
                    <div className="underline"></div>
                  </div>
                </div>
              </div>
              <div className="col-md-3">
                <div className="card shadow">
                  <img
                    src={profile4}
                    className="w-100 border-bottom"
                    alt="members"
                  />
                  <div className="card-body text-center">
                    <h6>Driver</h6>
                    <div className="underline"></div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </section>
      </div>
      <Footer/>
    </>
  );
}
export default Home;
