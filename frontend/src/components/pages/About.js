import React from 'react';
import bgvideo from '../../video/bgvideo.mp4'
import Service1 from '../images/enjoy.jpg'
import Service2 from '../images/seat.jpg'
import '../pages/About.css'
import Footer from '../inc/footer';
import Navbar from '../inc/Navbar';

// import Myimage from '../images/Buses.jpg'



function About() {
    return(
        <>
        <Navbar/>
        <div >
             <section className='section'>
             {/* <img src={Myimage} className='w-100 border-bottom' alt='Sevices'/> */}
             <video autoPlay loop muted className='video'>
            <source src={bgvideo} />
            </video>

               <div className='container'>
               
                    <div className='row'>
                   
                        <div className='col-md-12 text-center'>
                            <h3 className='main-heading'>Our Company</h3>
                            <div className='underline mx-auto'></div>
                           
                            <p>
                                We are committed to providing exceptional customer service and going above and beyond to meet our passengers' needs. We believe that a bus ride should be more than just a way to get from point A to point B – it should be an experience that leaves you feeling satisfied and fulfilled.
                            </p>
                           
                        </div>
                    </div>
                </div> 
            </section>
            {/* Vision Mission values */}
            <section className="section bg-c-light border-top">
            <div className="container">
                <div className="row">
                    <div className="col-md-12 mb-4 text-center">
                        <h3 className="main-heading">Vision Mission and Values</h3>
                        <div className="underline mx-auto"></div>
                    </div>
                    <div className="col-md-4 text-center">
                       <h5>Our Vision</h5>
                       <p>
                       Provide customers with a convenient and efficient way to book bus tickets from anywhere, anytime
                        </p>
 
                    </div>
                    <div className="col-md-4 text-center">
                       <h5>Our Mission</h5>
                       <p>
                       Provide a convenient and user-friendly platform for individuals and groups to easily book bus tickets for their travels. 
                        </p>
                    </div>
                    <div className="col-md-4 text-center">
                       <h5>Values</h5>
                       <p>
                       Commitment to providing a reliable, convenient, and sustainable mode of transportation for everyone.
                        </p>
 
                    </div>
                </div>
            </div>
        </section>
        
        {/* Our Services */}
        <section className="section bg-c-light mb-5 border-top ">
            <div className="container">
                <div className="row justify-content-center">
                    <div className="col-md-12 mb-4 text-center">
                        <h3 className="main-heading">Our Premier Bus Service</h3>
                        <div className="underline mx-auto"></div>
                    </div>
                    <div className="col-md-4">
                        <div className='card shadow'>
                        <img src={Service1} className='w-100 border-bottom' alt='Sevices'/>
                            <div className='card-body'>
                               <div className='underline'></div>
                               <p>Welcome aboard our bus service, where your comfort and convenience are our top priorities. We strive to make your journey enjoyable.</p> 
                            </div>
                        </div>
                    </div>
                    <div className="col-md-4">
                        <div className='card shadow'>
                        <img src={Service2} className='w-100 border-bottom' alt='Sevices'/>
                            <div className='card-body'>
                               <div className='underline'></div>
                               <p>Our buses are equipped with spacious and plush seating to ensure a comfortable ride. Relax during your journey with well-cushioned seats.</p> 
                            </div>
                        </div>
                    </div>  
                </div>
            </div>
        </section> 
        </div>
        <Footer/>
        </>
        
    )
}
export default About;