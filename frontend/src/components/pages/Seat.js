import React, { useState, useEffect } from "react";
import { Button } from "react-bootstrap";
import { MdEventSeat } from "react-icons/md";
import { TbSteeringWheel } from "react-icons/tb";
import { Link, useParams } from "react-router-dom";
import Footer from '../inc/footer';
import Navbar from '../inc/Navbar';

function Seat() {
  const { busID } = useParams();
  const [availableCount, setAvailableCount] = useState(0);
  const [selectedCount, setSelectedCount] = useState(0);
  const [selectedSeat, setSelectedSeat] = useState([]);
  const [seats, setSeats] = useState(Array(20).fill("available"));

  
  const countItems = (items) => {
    let available = 0;
    let selected = 0;

    items.forEach((item) => {
      if (item === "available") {
        available++;
      } else if (item === "selected") {
        selected++;
      }
    });

    setAvailableCount(available);
    setSelectedCount(selected);
  };

  const selectSeat = (index) => {
    if (seats[index] === "available") {
      seats[index] = "selected";
      setSelectedSeat([...selectedSeat, index]);
      setSeats([...seats]);
    } else if (seats[index] === "selected") {
      seats[index] = "available";
      setSelectedSeat((selectedSeat) =>
        selectedSeat.filter((ind) => ind !== index)
      );
      setSeats([...seats]);
    }
  };

  useEffect(() => {
    async function fetchData(){
      try{
        const res = await fetch(`http://localhost:8080/manage-ride/${busID}`);

        if (res.ok){
          const json = await res.json();

          setSelectedSeat(json.seats);
          const newSeats = seats;
          json.seats.forEach(seat => {
            newSeats[seat] = "booked";
          })
          setSeats(newSeats);
        }
      }
      catch (err){
        alert(err.message);
      }
    }

    fetchData();
  }, [])

  useEffect(() => {
    countItems(seats);
  }, [seats]);

  const renderSeat = (seat, index) => {
    // console.log("s", seat);
    let iconClass = "";
    switch (seat) {
      case "available":
        iconClass = "text-secondary";
        break;
      case "selected":
        iconClass = "text-success";
        break;
      case "booked":
        iconClass = "text-danger";
        break;
      default:
        break;
    }

    return (
      <button
        key={index}
        className={`border border-light bg-white  ${
          seat === "booked" ? "disabled" : ""
        }`}
        disabled={seat === "booked" ? true : false}
        onClick={() => selectSeat(index)}
      >
        <MdEventSeat className={iconClass} style={{ fontSize: 60 }} />
      </button>
    );
  };

  return (
    <>
    <Navbar/>
    <div className="container-fluid py-5">
      <h2 className="text-center mb-3">Bus booking</h2>
      <div className="text-center">
      <div className="d-inline-flex align-items-center me-5">
        <MdEventSeat size={40} className="mb-2 text-secondary" />
        <span>Available</span>
      </div>
      <div className="d-inline-flex align-items-center me-5">
        <MdEventSeat size={40} className="mb-2 text-success" />
        <span>Selected</span>
      </div>
      <div className="d-inline-flex align-items-center me-5">
        <MdEventSeat size={40} className="mb-2 text-danger" />
        <span>Booked</span>
      </div>
    </div>
      <div className=" container my-5" style={{maxHeight: 700}}>
        <main class="d-flex flex-column border boder-dark " style={{maxHeight: 700}}>
          <div class="row justify-content-md-center " >
            <div class=" col-1 ">
              {seats.map((seat, index) =>
                index == 1 ? renderSeat(seat, index) : <div></div>
              )}
            </div>
            <div class=" col-1 "></div>
            <div class=" col-1 "></div>
            <div class=" col-1 ">
              <TbSteeringWheel
                className="text-primary"
                style={{ fontSize: 60 }}
              />
            </div> 
          </div>
          <div class="row justify-content-md-center">
            <div class=" col-1 ">
              {seats.map((seat, index) =>
                index == 2 ? renderSeat(seat, index) : <div></div>
              )}
            </div>
            <div class=" col-1 "></div>
            <div class=" col-1 ">
              {seats.map((seat, index) =>
                index == 3 ? renderSeat(seat, index) : <div></div>
              )}
            </div>
            <div class=" col-1 ">
              {seats.map((seat, index) =>
                index == 4 ? renderSeat(seat, index) : <div></div>
              )}
            </div>
          </div>
          <div class="row justify-content-md-center">
            <div class=" col-1 "></div>
            <div class=" col-1 "></div>
            <div class=" col-1 ">
              {seats.map((seat, index) =>
                index == 5 ? renderSeat(seat, index) : <div></div>
              )}
            </div>
            <div class=" col-1 ">
              {seats.map((seat, index) =>
                index == 6 ? renderSeat(seat, index) : <div></div>
              )}
            </div>
          </div>
          <div class="row justify-content-md-center">
            <div class=" col-1 ">
              {seats.map((seat, index) =>
                index == 7 ? renderSeat(seat, index) : <div></div>
              )}
            </div>
            <div class=" col-1 "></div>
            <div class=" col-1 ">
              {seats.map((seat, index) =>
                index == 8 ? renderSeat(seat, index) : <div></div>
              )}
            </div>
            <div class=" col-1 ">
              {seats.map((seat, index) =>
                index == 9 ? renderSeat(seat, index) : <div></div>
              )}
            </div>
          </div>
          <div class="row justify-content-md-center">
            <div class=" col-1 ">
              {seats.map((seat, index) =>
                index == 10 ? renderSeat(seat, index) : <div></div>
              )}
            </div>
            <div class=" col-1 "></div>
            <div class=" col-1 ">
              {seats.map((seat, index) =>
                index == 11 ? renderSeat(seat, index) : <div></div>
              )}
            </div>
            <div class=" col-1 ">
              {seats.map((seat, index) =>
                index == 12 ? renderSeat(seat, index) : <div></div>
              )}
            </div>
          </div>
          <div class="row justify-content-md-center">
            <div class=" col-1 ">
              {seats.map((seat, index) =>
                index == 13 ? renderSeat(seat, index) : <div></div>
              )}
            </div>
            <div class=" col-1 "></div>
            <div class=" col-1 ">
              {seats.map((seat, index) =>
                index == 14 ? renderSeat(seat, index) : <div></div>
              )}
            </div>
            <div class=" col-1 ">
              {seats.map((seat, index) =>
                index == 15 ? renderSeat(seat, index) : <div></div>
              )}
            </div>
          </div>
          <div class="row justify-content-md-center">
            <div class=" col-1 ">
              {seats.map((seat, index) =>
                index == 16 ? renderSeat(seat, index) : <div></div>
              )}
            </div>
            <div class=" col-1 ">
              {seats.map((seat, index) =>
                index == 17 ? renderSeat(seat, index) : <div></div>
              )}
            </div>
            <div class=" col-1 ">
              {seats.map((seat, index) =>
                index == 18 ? renderSeat(seat, index) : <div></div>
              )}
            </div>
            <div class=" col-1 ">
              {seats.map((seat, index) =>
                index == 19 ? renderSeat(seat, index) : <div></div>
              )}
            </div>
          </div>
        </main>
      </div>
      <div className="d-flex justify-content-center mt-4">
        <div className="d-flex flex-column align-items-center mx-3">
          <i className="bi bi-circle mx-2"></i>
          <span>Available: {availableCount}</span>
        </div>
        <div className="d-flex">
          Selected :
          {seats.map((type, index) => {return {type, index}}).filter(seat => seat.type === "selected").map(seat => seat.index).join(",")}
        </div>
{/* 
        <Link to="/" className="text">
          <button>
          Confirm booking
          </button>
          
        </Link>  */}
      
     
        {/* <div className="d-flex flex-column align-items-center mx-3">
            <i className="bi bi-check-circle-fill mx-2"></i>
            <span>selected {selectedCount}</span>
            </div> */}
      </div>
    
      <div className="text-center w-1">
        <Link 
        to={{
              pathname: `/userdetail/${busID}/${seats.map((type, index) => {return {type, index}}).filter(seat => seat.type === "selected").map(seat => seat.index).join(",")}`
            }}
            className="btn btn-primary"
          >
            Booked Now
        </Link>
  </div> 
 
</div>
    <Footer/>
    
    </>
  );
}
export default Seat;

