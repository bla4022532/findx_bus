import React, { useState, useEffect } from 'react';
import { Button, Modal } from 'react-bootstrap';

function AddUser() {
  const [users, setUsers] = useState([]);
  const [showModal, setShowModal] = useState(false);
  const [selectedUserId, setSelectedUserId] = useState(null);
  const [deleteSuccess, setDeleteSuccess] = useState(false);

  useEffect(() => {
    fetchData();
  }, []);

  const fetchData = async () => {
    try {
      const response = await fetch('http://localhost:8080/feedback', {
        method: 'GET',
        headers: {
          'Content-Type': 'application/json',
        },
      });

      if (response.ok) {
        const userData = await response.json();

        if (Array.isArray(userData)) {
          setUsers(userData);
        } else {
          console.error('Invalid user data:', userData);
        }
      }
    } catch (error) {
      console.error(error);
    }
  };

  const handleDelete = async (id) => {
    try {
      const response = await fetch(`http://localhost:8080/feedback`, {
        method: 'DELETE',
        headers: {
          'Content-Type': 'application/json',
        },
        body: JSON.stringify({ id }),
      });

      if (response.ok) {
        // If the deletion was successful, remove the deleted entry from the users state
        fetchData();
        setDeleteSuccess(true);
        setShowModal(false);

        // Reset delete success after 2 seconds
        setTimeout(() => {
          setDeleteSuccess(false);
        }, 2000);
      } else {
        console.error('Delete request failed');
      }
    } catch (error) {
      console.error(error);
    }
  };

  const handleOpenDeleteModal = (id) => {
    setSelectedUserId(id);
    setShowModal(true);
  };

  const handleCancelDelete = () => {
    setShowModal(false);
  };

  return (
    <div className="table-responsive m-2">
      <h2 className='text-center'>Feedback</h2>
      <table className="table table-secondary table-hover mt-2">
        <thead>
          <tr>
            <th scope="col">Full Name</th>
            <th scope="col">Phone Number</th>
            <th scope="col">Email Address</th>
            <th scope="col">Feedback</th>
            <th scope="col"></th>
          </tr>
        </thead>
        <tbody>
          {users.map((user) => (
            <tr key={user.id}>
              <td>{user.username}</td>
              <td>{user.phonenumber}</td>
              <td>{user.email}</td>
              <td>{user.feedback}</td>
              <td>
                <a
                  href="#"
                  className="text-danger"
                  data-toggle="tooltip"
                  data-placement="top"
                  title="Delete"
                  onClick={() => handleOpenDeleteModal(user.id)}
                >
                  <i className="fas fa-trash-alt mt-2"></i>
                </a>
              </td>
            </tr>
          ))}
        </tbody>
      </table>

      <Modal show={showModal} onHide={handleCancelDelete}>
        <Modal.Header closeButton>
          <Modal.Title>Confirmation</Modal.Title>
        </Modal.Header>
        <Modal.Body>Are you sure you want to delete this user's feedback?</Modal.Body>
        <Modal.Footer>
          <div className="d-flex justify-content-center">
            <Button variant="secondary" onClick={handleCancelDelete}>
              Cancel
            </Button>
            <span className="mx-2"></span>
            <Button variant="danger" onClick={() => handleDelete(selectedUserId)}>
              Confirm
            </Button>
          </div>
        </Modal.Footer>
      </Modal>

      {deleteSuccess && (
        <div className="alert alert-success" role="alert">
          Delete successful
        </div>
      )}
    </div>
  );
}

export default AddUser;

// import React, { useState, useEffect } from 'react';

// function AddUser() {
//   const [users, setUsers] = useState([]);

//   useEffect(() => {
//     fetchData();
//   }, []);

//   const fetchData = async () => {
//     try {
//       const response = await fetch('http://localhost:8080/feedback', {
//         method: 'GET',
//         headers: {
//           'Content-Type': 'application/json',
//         },
//       });

//       if (response.ok) {
//         const userData = await response.json();

//         if (Array.isArray(userData)) {
//           setUsers(userData);
//         } else {
//           console.error('Invalid user data:', userData);
//         }
//       }
//     } catch (error) {
//       console.error(error);
//     }
//   };

//   const handleDelete = async (id) => {
//     try {
//       const response = await fetch(`http://localhost:8080/feedback`, {
//         method: 'DELETE',
//         headers: {
//           'Content-Type': 'application/json',
//         },
//         body: JSON.stringify({id})
//       });

//       if (response.ok) {
//         // If the deletion was successful, remove the deleted entry from the users state
//         fetchData()
//         alert("delete successful")
//       } else {
//         console.error('Delete request failed');
//         alert('failure')
//       }
//     } catch (error) {
//       console.error(error);
//     }
//   };

//   return (
//     <div className="table-responsive m-5">
//       <table className="table table-secondary table-hover mt-5">
//         <thead>
//           <tr>
//             <th scope="col">Full Name</th>
//             <th scope="col">Phone Number</th>
//             <th scope="col">Email Address</th>
//             <th scope="col">Feedback</th>
//             <th scope="col">hee</th>
//           </tr>
//         </thead>
//         <tbody>
//           {users.map((user) => (
//             <tr key={user.id}>
//               <td>{user.username}</td>
//               <td>{user.phonenumber}</td>
//               <td>{user.email}</td>
//               <td>{user.feedback}</td>
//               <td>
//                 <a
//                   href="#"
//                   className="text-danger"
//                   data-toggle="tooltip"
//                   data-placement="top"
//                   title="Delete"
//                   onClick={() => handleDelete(user.id)}
//                 >
//                   <i className="fas fa-trash-alt"></i>
//                 </a>
//               </td>
//             </tr>
//           ))}
//         </tbody>
//       </table>
//     </div>
//   );
// }

// export default AddUser;
