import React, { useState, useEffect } from 'react';
import { Button, Modal } from 'react-bootstrap';
import './Viewseat.css';
import { Link } from 'react-router-dom';

function ManageRides() {
  const [busData, setBusData] = useState([]);
  const [showModal, setShowModal] = useState(false);
  const [selectedBusId, setSelectedBusId] = useState(null);
  const [deleteSuccess, setDeleteSuccess] = useState(false);

  useEffect(() => {
    fetchData();
  }, []);

  const fetchData = async () => {
    try {
      const response = await fetch('http://localhost:8080/create-bus', {
        method: 'GET',
        headers: {
          'Content-Type': 'application/json',
        },
      });
      const jsonData = await response.json();
      setBusData(jsonData);
      console.log(jsonData);
    } catch (error) {
      console.error(error);
    }
  };

  const handleDelete = async (busID) => {
    try {
      const response = await fetch(`http://localhost:8080/create-bus`, {
        method: 'DELETE',
        headers: {
          'Content-Type': 'application/json',
        },
        body: JSON.stringify({ busID }),
      });

      if (response.ok) {
        // If the deletion was successful, remove the deleted bus from the busData state
        fetchData();
        setDeleteSuccess(true);
        setShowModal(false);

        // Reset delete success after 2 seconds
        setTimeout(() => {
          setDeleteSuccess(false);
        }, 2000);
      } else {
        console.error('Failed to delete the bus');
      }
    } catch (error) {
      console.error(error);
    }
  };

  const handleOpenDeleteModal = (busID) => {
    setSelectedBusId(busID);
    setShowModal(true);
  };

  const handleCancelDelete = () => {
    setShowModal(false);
  };

  const formatTime = (time) => {
    const [hours, minutes] = time.split(':');
    let formattedTime = hours + ':' + minutes;

    const hour = parseInt(hours);
    if (hour < 12) {
      formattedTime += ' AM';
    } else {
      formattedTime += ' PM';
    }

    return formattedTime;
  };

  return (
    <>
      <h3 className="text-center space-after-h3">Manage Booking</h3>
      <div className="container center-table">
        <div className="bus-info ">
          <div className="table-responsive ">
            <table className="table" id="t2">
              <thead>
                <tr>
                  <th>Bus Name</th>
                  <th>Departure Time</th>
                  <th>Total seats</th>
                  <th>Price per seat</th>
                  <th>Date</th>
                  <th>From</th>
                  <th>To</th>
                  <th>Manage ride</th>
                  <th></th>
                </tr>
              </thead>
              <tbody className="main-table">
                {busData.map((bus) => (
                  <tr key={bus.id}>
                    <td>{bus.busName}</td>
                    <td>{formatTime(bus.DepatureTime)}</td>
                    <td>{bus.totalseat}</td>
                    <td>{bus.price_per_seat}</td>
                    <td>{bus.date}</td>
                    <td>{bus.from}</td>
                    <td>{bus.to}</td>
                    <td>
                      <button className="center">
                        <Link to={{ pathname: `/manageride/${bus.id}` }} style={{ textDecoration: 'none' }}>
                          Manage ride
                        </Link>
                      </button>
                    </td>
                    <td>
                      <a
                        href="#"
                        className="text-danger delete-icon"
                        onClick={() => handleOpenDeleteModal(bus.id)}
                        data-toggle="tooltip"
                        data-placement="top"
                        title="Delete"
                      >
                        <i className="fas fa-trash-alt mt-2"></i>
                      </a>
                    </td>
                  </tr>
                ))}
              </tbody>
            </table>
          </div>
        </div>
      </div>

      <Modal show={showModal} onHide={handleCancelDelete}>
        <Modal.Header closeButton>
          <Modal.Title>Confirmation</Modal.Title>
        </Modal.Header>
        <Modal.Body>Are you sure you want to delete this bus?</Modal.Body>
        <Modal.Footer>
          <div className="d-flex justify-content-between">
            <Button variant="secondary" onClick={handleCancelDelete}>
              Cancel
            </Button>
            <span className="mx-2"></span>
            <Button variant="danger" onClick={() => handleDelete(selectedBusId)}>
              Confirm
            </Button>
          </div>
        </Modal.Footer>
      </Modal>

      {deleteSuccess && (
        <div className="alert alert-success" role="alert">
          Delete successful
        </div>
      )}
    </>
  );
}

export default ManageRides;


// import React, { useState, useEffect } from 'react';
// import './Viewseat.css';
// import { Link } from 'react-router-dom';

// function ManageRides() {
//   const [busData, setBusData] = useState([]);

//   useEffect(() => {
//     fetchData();
//   }, []);

//   const fetchData = async () => {
//     try {
//       const response = await fetch('http://localhost:8080/create-bus', {
//         method: 'GET',
//         headers: {
//           'Content-Type': 'application/json',
//         },
//       });
//       const jsonData = await response.json();
//       setBusData(jsonData);
//       console.log(jsonData);
//     } catch (error) {
//       console.error(error);
//     }
//   };

//   const handleDelete = async (busID) => {
//     const confirmed = window.confirm('Are you sure you want to delete this bus?');

//     if (confirmed) {
//       try {
//         const response = await fetch(`http://localhost:8080/create-bus`, {
//           method: 'DELETE',
//           headers: {
//             'Content-Type': 'application/json',
//           },
//           body: JSON.stringify({busID})
//         });

//         if (response.ok) {
//           // Delete operation successful, update the state or perform any additional actions
//           alert("bus deleted")
//           fetchData();
//         } else {
//           alert("failure")
//           console.error('Failed to delete the bus');
//         }
//       } catch (error) {
//         console.error(error);
//       }
//     }
//   };

//   return (
//     <>
//       <h3 className="text-center space-after-h3">Manage Booking</h3>
//       <div className="container center-table mt-5">
//         <div className="bus-info mt-5">
//           <div className="table-responsive">
//             <table className="table" id="t2">
//               <thead>
//                 <tr>
//                   <th>Bus Name</th>
//                   <th>Departure Time</th>
//                   <th>Total seats</th>
//                   <th>Price per seat</th>
//                   <th>Date</th>
//                   <th>From</th>
//                   <th>To</th>
//                   <th>Make a choice</th>
//                   <th>Delete</th>
//                 </tr>
//               </thead>
//               <tbody className="main-table">
//                 {busData.map((bus) => (
//                   <tr key={bus.id}>
//                     <td>{bus.busName}</td>
//                     <td>{bus.DepatureTime}</td>
//                     <td>{bus.totalseat}</td>
//                     <td>{bus.price_per_seat}</td>
//                     <td>{bus.date}</td>
//                     <td>{bus.from}</td>
//                     <td>{bus.to}</td>
//                     <td>
//                       <button className="center">
//                         <Link to={{ pathname: `/manageride/${bus.id}` }} style={{ textDecoration: 'none' }}>
//                           Manage ride
//                         </Link>
//                       </button>
//                     </td>
//                     <td>
//                       <a
//                         href="#"
//                         className="text-danger delete-icon"
//                         onClick={() => handleDelete(bus.id)}
//                         data-toggle="tooltip"
//                         data-placement="top"
//                         title="Delete"
//                       >
//                         <i className="fas fa-trash-alt"></i>
//                       </a>
//                     </td>
//                   </tr>
//                 ))}
//               </tbody>
//             </table>
//           </div>
//         </div>
//       </div>
//     </>
//   );
// }

// export default ManageRides;
