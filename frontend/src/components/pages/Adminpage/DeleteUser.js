import React from "react";

function DeleteUser(){
    return<div className="table-responsive m-5">
    <table className="table table-secondary table-hover">
      <thead>
        <tr>
          <th scope="col">Name</th>
          <th scope="col">Email</th>
          <th scope="col">CID</th>
        </tr>
      </thead>
      <tbody>
        <tr>
          <td>Dawa</td>
          <td>12200001.gcit@rub.edu.bt</td>
          <td>11509004959</td>
          
        </tr>
        <tr>
          <td>Pema</td>
          <td>12200001.gcit@rub.edu.bt</td>
          <td>11509004959</td>
          
        </tr>
        <tr>
          <td>santay</td>
          <td>12200001.gcit@rub.edu.bt</td>
          <td>11509004959</td>
          
        </tr>
        
      </tbody>
    </table>
  </div>
}

export default DeleteUser;