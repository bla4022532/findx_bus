import React from "react";
import { BrowserRouter as Router, Routes, Route, Link, useNavigate, Outlet } from "react-router-dom";
import "./Admin.css";
// import mylogo from "../../images/Buslogo.jpg"

import DeleteUser from "./DeleteUser";
import AddUser from "./AddUser";
import CreateRide from "./CreateRide";
import ManageRide from "./ManageRide";

const App = () => {
  const navigate = useNavigate();

  return (
    <div class="d-flex">
      <div
        class="flex-shrink-0 p-3 bg-white shadow-lg vh-100"
        style={{ width: 200, left: 0, top: 0}}
      >
        <a
          href="/"
          class="d-flex align-items-center pb-3 mb-3 link-dark text-decoration-none border-bottom"
        >
          {/* <svg class="bi me-2" width="30" height="24"><use xlink:href="#bootstrap"/></svg> */}
          <div class="d-flex align-items-center">
          {/* <img src={mylogo} className='logo' alt='Sevices'/>  */}
            <span class="fs-5 fw-semibold">EasyBus</span>
        </div>

        </a>
        <ul class="list-unstyled ps-0">
          <li class="mb-1">
            <div class="d-flex align-items-center justify-content-between">
              <button
                class="btn btn-toggle rounded collapsed"
                data-bs-toggle="collapse"
                data-bs-target="#home-collapse"
                aria-expanded="true"
              >
                Ride
              </button>
              <span></span>
            </div>
            <div class="collapse show" id="home-collapse">
              <ul class="btn-toggle-nav list-unstyled fw-normal pb-1 small">
                <li>
                  <Link to="createride" class="link-dark rounded">
                    Create Ride
                  </Link>
                </li>
                <li>
                  <Link to="managerides" class="link-dark rounded">
                    Manage Rides
                  </Link>
                </li>
              </ul>
            </div>
          </li>

          <li class="mb-1">
            <div class="d-flex align-items-center justify-content-between">
              <button
                class="btn btn-toggle rounded collapsed"
                data-bs-toggle="collapse"
                data-bs-target="#orders-collapse"
                aria-expanded="false"
              >
                Users
              </button>
              <span></span>
            </div>
            <div class="collapse" id="orders-collapse">
              <ul class="btn-toggle-nav list-unstyled fw-normal pb-1 small">
                <li>
                  <Link to="adduser" class="link-dark rounded">
                    Feedbacks
                  </Link>
                </li>
              </ul>
            </div>
          </li>
          <li class="border-top my-3"></li>
          <li class="mb-1">
          <div className="text-center mt-2">
          <button onClick={() => {navigate("/admin", {replace: true})}} className="link-style text-decoration-none text-start" >
            Logout
          </button>

           </div>
          </li>
        </ul>
      </div>
      <div class="w-100 h-100">
        <nav class="navbar navbar-expand-lg navbar-light shadow-lg bg-light">
          <div class="container-fluid d-flex justify-content-center">
            <a class="navbar-brand text-dark" href="#">
              {/* Dashboard */}
            </a>
          </div>
        </nav>
        {/* YOUR COMPONEST GOES HERE */}
        <Outlet/>
      </div>
    </div>
  );
};

export default App;

