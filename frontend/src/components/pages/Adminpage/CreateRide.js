import React, { useState } from "react";
import { useNavigate } from 'react-router-dom';

const CreateRide = () => {
  const [date, setDate] = useState('');
  const [DepatureTime, setDepatureTime] = useState();
  const [from, setFrom] = useState('');
  const [to, setTo] = useState('');
  const [totalseat, settotalseat] = useState(0);
  const [busName, setBusName] = useState('');
  const [price_per_seat, setprice_per_seat] = useState(0);

  const [error, setError] = useState('');
  const navigate = useNavigate();

  const onSubmitForm = async (e) => {
    e.preventDefault();

    if (!busName || !DepatureTime || !totalseat || !price_per_seat || !date || !from || !to) {
      setError('All fields are required');
      return;
    }

    try {
      const depatureTime12Hour = new Date(`2000-01-01T${DepatureTime}`);
      const depatureTimeFormatted = depatureTime12Hour.toLocaleTimeString([], { hour: '2-digit', minute: '2-digit', hour12: true });

      const datas = {
        busName,
        DepatureTime: depatureTimeFormatted,
        totalseat,
        price_per_seat,
        date,
        from,
        to,
      };

      const response = await fetch('http://localhost:8080/create-bus', {
        method: 'POST',
        headers: {
          'Content-Type': 'application/json'
        },
        body: JSON.stringify(datas)
      });

      if (response.ok) {
        const responseData = await response.json();
        console.log('Bus created:', responseData);
        navigate('/add');
      } else {
        const errorData = await response.json();
        console.error('Failed to create bus:', errorData.message);
      }
    } catch (error) {
      console.error('An error occurred:', error);
    }
  };

  const handleDateChange = (e) => {
    const selectedDate = new Date(e.target.value);
    const currentDate = new Date();

    if (selectedDate < currentDate) {
      setDate('');
      setError('Please select a current or future date');
    } else {
      setDate(e.target.value);
      setError('');
    }
  };

  return (
    <div className="container py-5 h-100">
      <h1 className="text-center mb-3 text-red">Create Bus</h1>
      <form onSubmit={onSubmitForm}>
        {error && <p style={{ color: 'red' }}>{error}</p>}
        <div className="row">
          <div className="col-md-6 mb-2">
            <label htmlFor="busName" className="form-label">Bus Name</label>
            <input
              type="text"
              className="form-control"
              id="busName"
              name="name"
              value={busName}
              onChange={(e) => setBusName(e.target.value)}
            />
          </div>
          <div className="col-md-6 mb-2">
            <label htmlFor="DepatureTime" className="form-label">Departure Time</label>
            <input
              type="time"
              className="form-control"
              id="DepatureTime"
              name="time"
              value={DepatureTime}
              onChange={(e) => setDepatureTime(e.target.value)}
            />
          </div>
        </div>
        <div className="row">
          <div className="col-md-6 mb-2">
            <label htmlFor="totalseat" className="form-label">Total Seats</label>
            <input
              type="number"
              className="form-control"
              id="totalseat"
              name="seats"
              value={totalseat}
              onChange={(e) => settotalseat(parseInt(e.target.value))}
            />
          </div>
          <div className="col-md-6 mb-2">
            <label htmlFor="price_per_seat" className="form-label">Price per Seat</label>
            <input
              type="number"
              className="form-control"
              id="price_per_seat"
              name="price_per_seat"
              value={price_per_seat}
              onChange={(e) => setprice_per_seat(e.target.value)}
            />
          </div>
        </div>
        <div className="row">
          <div className="col-md-6 mb-2">
            <label htmlFor="date" className="form-label">Date</label>
            <input
              type="date"
              className={`form-control ${date < new Date().toISOString().split('T')[0] ? 'blur' : ''}`}
              id="date"
              name="date"
              value={date}
              onChange={handleDateChange}
            />
          </div>
          <div className="col-md-6 mb-2">
            <label htmlFor="from" className="form-label">From</label>
            <input
              type="text"
              className="form-control"
              id="from"
              name="from"
              value={from}
              onChange={(e) => setFrom(e.target.value)}
            />
          </div>
        </div>
        <div className="row">
          <div className="col-md-6 mb-2">
            <label htmlFor="to" className="form-label">To</label>
            <input
              type="text"
              className="form-control"
              id="to"
              name="to"
              value={to}
              onChange={(e) => setTo(e.target.value)}
            />
          </div>
        </div>

        <div className="text-center">
          <button type="submit" className="btn btn-primary">Create</button>
        </div>
      </form>
    </div>
  );
};

export default CreateRide;

// import React, { useState } from "react";
// import { useNavigate } from 'react-router-dom';

// const CreateRide = () => {
//   const [date, setDate] = useState('');
//   const [DepatureTime, setDepatureTime] = useState();
//   const [from, setFrom] = useState('');
//   const [to, setTo] = useState('');
//   const [totalseat, settotalseat] = useState(0);
//   const [busName, setBusName] = useState('');
//   const [price_per_seat, setprice_per_seat] = useState(0);

//   const [error, setError] = useState('');
//   const navigate = useNavigate();

//   const onSubmitForm = async (e) => {
//     e.preventDefault();

//     if (!busName || !DepatureTime || !totalseat || !price_per_seat || !date || !from || !to) {
//       setError('All fields are required');
//       return;
//     }

//     try {
//       const depatureTime12Hour = new Date(`2000-01-01T${DepatureTime}`);
//       const depatureTimeFormatted = depatureTime12Hour.toLocaleTimeString([], { hour: '2-digit', minute: '2-digit', hour12: true });

//       const datas = {
//         busName,
//         DepatureTime: depatureTimeFormatted,
//         totalseat,
//         price_per_seat,
//         date,
//         from,
//         to,
//       };

//       const response = await fetch('http://localhost:8080/create-bus', {
//         method: 'POST',
//         headers: {
//           'Content-Type': 'application/json'
//         },
//         body: JSON.stringify(datas)
//       });

//       if (response.ok) {
//         const responseData = await response.json();
//         console.log('Bus created:', responseData);
//         navigate('/add');
//       } else {
//         const errorData = await response.json();
//         console.error('Failed to create bus:', errorData.message);
//       }
//     } catch (error) {
//       console.error('An error occurred:', error);
//     }
//   };

//   return (
//     <div className="container py-5 h-100">
//       <h1 className="text-center mb-3 text-red">Create Bus</h1>
//       <form onSubmit={onSubmitForm}>
//         {error && <p>{error}</p>}
//         <div className="row">
//           <div className="col-md-6 mb-2">
//             <label htmlFor="busName" className="form-label">Bus Name</label>
//             <input
//               type="text"
//               className="form-control"
//               id="busName"
//               name="name"
//               value={busName}
//               onChange={(e) => setBusName(e.target.value)}
//             />
//           </div>
//           <div className="col-md-6 mb-2">
//             <label htmlFor="DepatureTime" className="form-label">Departure Time</label>
//             <input
//               type="time"
//               className="form-control"
//               id="DepatureTime"
//               name="time"
//               value={DepatureTime}
//               onChange={(e) => setDepatureTime(e.target.value)}
//             />
//           </div>
//         </div>
//         <div className="row">
//           <div className="col-md-6 mb-2">
//             <label htmlFor="totalseat" className="form-label">Total Seats</label>
//             <input
//               type="number"
//               className="form-control"
//               id="totalseat"
//               name="seats"
//               value={totalseat}
//               onChange={(e) => settotalseat(parseInt(e.target.value))}
//             />
//           </div>
//           <div className="col-md-6 mb-2">
//             <label htmlFor="price_per_seat" className="form-label">Price per Seat</label>
//             <input
//               type="number"
//               className="form-control"
//               id="price_per_seat"
//               name="price_per_seat"
//               value={price_per_seat}
//               onChange={(e) => setprice_per_seat(e.target.value)}
//             />
//           </div>
//         </div>
//         <div className="row">
//           <div className="col-md-6 mb-2">
//             <label htmlFor="date" className="form-label">Date</label>
//             <input
//               type="date"
//               className="form-control"
//               id="date"
//               name="date"
//               value={date}
//               onChange={(e) => setDate(e.target.value)}
//             />
//           </div>
//           <div className="col-md-6 mb-2">
//             <label htmlFor="from" className="form-label">From</label>
//             <input
//               type="text"
//               className="form-control"
//               id="from"
//               name="from"
//               value={from}
//               onChange={(e) => setFrom(e.target.value)}
//             />
//           </div>
//         </div>
//         <div className="row">
//           <div className="col-md-6 mb-2">
//             <label htmlFor="to" className="form-label">To</label>
//             <input
//               type="text"
//               className="form-control"
//               id="to"
//               name="to"
//               value={to}
//               onChange={(e) => setTo(e.target.value)}
//             />
//           </div>
//         </div>

//         <div className="text-center">
//           <button type="submit" className="btn btn-primary">Create</button>
//         </div>
//       </form>
//     </div>
//   );
// };

// export default CreateRide;

