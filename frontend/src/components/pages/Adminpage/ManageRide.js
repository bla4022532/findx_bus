import React, { useState, useEffect } from "react";
import { Button } from "react-bootstrap";
import { MdEventSeat } from "react-icons/md";
import { TbSteeringWheel } from "react-icons/tb";
import { Link, useParams } from "react-router-dom";
import { ToastContainer, toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";
//import Footer from '../inc/footer';
// import Navbar from '../inc/Navbar';

function ManageRide() {
  const { busID } = useParams();
  const [availableCount, setAvailableCount] = useState(0);
  const [selectedCount, setSelectedCount] = useState(0);
  const [passengerDetails, setPassengerDetails] = useState([]);
  const [selectedSeat, setSelectedSeat] = useState([]);
  const [seats, setSeats] = useState(Array(20).fill("available"));
  const [modalData, setModalData] = useState({title: "", body: ""})

  const countItems = (items) => {
    let available = 0;
    let selected = 0;

    items.forEach((item) => {
      if (item === "available") {
        available++;
      } else if (item === "selected") {
        selected++;
      }
    });

    setAvailableCount(available);
    setSelectedCount(selected);
  };

  const selectSeat = (index) => {
    if (seats[index] === "available") {
      seats[index] = "selected";
      setSelectedSeat([...selectedSeat, index]);
      setSeats([...seats]);
      notifyAdmin("selected", index);
    } else if (seats[index] === "selected") {
      seats[index] = "available";
      setSelectedSeat((selectedSeat) =>
        selectedSeat.filter((ind) => ind !== index)
      );
      setSeats([...seats]);
      notifyAdmin("deselected", index);
    }
  };

  const notifyAdmin = (action, seatNumber) => {
    const message =
      action === "selected"
        ? `Seat ${seatNumber} has been selected.`
        : `Seat ${seatNumber} has been deselected.`;

    toast.info(message, {
      position: toast.POSITION.TOP_RIGHT,
      autoClose: 3000,
    });
  };

  useEffect(() => {
    async function fetchData(){
      try{
        const res = await fetch(`http://localhost:8080/manage-ride/${busID}`);

        if (res.ok){
          const json = await res.json();

          setPassengerDetails(json.passengerDetails)
          setSelectedSeat(json.seats);
          const newSeats = seats;
          json.seats.forEach(seat => {
            newSeats[seat] = "booked";
          })
          setSeats(newSeats);
        }
      }
      catch (err){
        alert(err.message);
      }
    }

    fetchData();
  }, [])

  useEffect(() => {
    countItems(seats);
  }, [seats]);

  const renderSeat = (seat, index) => {
    let iconClass = "";
    switch (seat) {
      case "available":
        iconClass = "text-secondary";
        break;
      case "selected":
        iconClass = "text-success";
        break;
      case "booked":
        iconClass = "text-danger";
        break;
      default:
        break;
    }

    return (
      <button
        key={index}
        className={`border border-light bg-white ${
          seat === "booked" ? "disabled" : ""
        }`}
        onClick={() => {
          passengerDetails.forEach(details => {
            if (details.seats.includes(index)){
              setModalData({title: `Seat ${index}`, 
              body: 
              <>
                <div>Name: {details.name}</div>
                <div>Email: {details.email}</div>
                <div>CID: {details.cid}</div>
              </>
            })
            }
          })
          if (seat !== "booked") setModalData({title: `Seat ${index}`, body: "Seat not booked"})
        }}
        type="button" data-bs-toggle="modal" data-bs-target="#exampleModal"
      >
        <MdEventSeat className={iconClass} style={{ fontSize: 60 }} />
      </button>
    );
  };

  return (
    <>
    <div className="container-fluid py-5">
      <h2 className="text-center">Bus booking</h2>

      <div className=" container my-5" style={{maxHeight: 700}}>
        <main class="d-flex flex-column border boder-dark " style={{maxHeight: 700}}>
          <div class="row justify-content-md-center " >
            <div class=" col-1 ">
              {seats.map((seat, index) =>
                index == 1 ? renderSeat(seat, index) : <div></div>
              )}
            </div>
            <div class=" col-1 "></div>
            <div class=" col-1 "></div>
            <div class=" col-1 ">
              <TbSteeringWheel
                className="text-primary"
                style={{ fontSize: 60 }}
              />
            </div> 
          </div>
          <div class="row justify-content-md-center">
            <div class=" col-1 ">
              {seats.map((seat, index) =>
                index == 2 ? renderSeat(seat, index) : <div></div>
              )}
            </div>
            <div class=" col-1 "></div>
            <div class=" col-1 ">
              {seats.map((seat, index) =>
                index == 3 ? renderSeat(seat, index) : <div></div>
              )}
            </div>
            <div class=" col-1 ">
              {seats.map((seat, index) =>
                index == 4 ? renderSeat(seat, index) : <div></div>
              )}
            </div>
          </div>
          <div class="row justify-content-md-center">
            <div class=" col-1 "></div>
            <div class=" col-1 "></div>
            <div class=" col-1 ">
              {seats.map((seat, index) =>
                index == 5 ? renderSeat(seat, index) : <div></div>
              )}
            </div>
            <div class=" col-1 ">
              {seats.map((seat, index) =>
                index == 6 ? renderSeat(seat, index) : <div></div>
              )}
            </div>
          </div>
          <div class="row justify-content-md-center">
            <div class=" col-1 ">
              {seats.map((seat, index) =>
                index == 7 ? renderSeat(seat, index) : <div></div>
              )}
            </div>
            <div class=" col-1 "></div>
            <div class=" col-1 ">
              {seats.map((seat, index) =>
                index == 8 ? renderSeat(seat, index) : <div></div>
              )}
            </div>
            <div class=" col-1 ">
              {seats.map((seat, index) =>
                index == 9 ? renderSeat(seat, index) : <div></div>
              )}
            </div>
          </div>
          <div class="row justify-content-md-center">
            <div class=" col-1 ">
              {seats.map((seat, index) =>
                index == 10 ? renderSeat(seat, index) : <div></div>
              )}
            </div>
            <div class=" col-1 "></div>
            <div class=" col-1 ">
              {seats.map((seat, index) =>
                index == 11 ? renderSeat(seat, index) : <div></div>
              )}
            </div>
            <div class=" col-1 ">
              {seats.map((seat, index) =>
                index == 12 ? renderSeat(seat, index) : <div></div>
              )}
            </div>
          </div>
          <div class="row justify-content-md-center">
            <div class=" col-1 ">
              {seats.map((seat, index) =>
                index == 13 ? renderSeat(seat, index) : <div></div>
              )}
            </div>
            <div class=" col-1 "></div>
            <div class=" col-1 ">
              {seats.map((seat, index) =>
                index == 14 ? renderSeat(seat, index) : <div></div>
              )}
            </div>
            <div class=" col-1 ">
              {seats.map((seat, index) =>
                index == 15 ? renderSeat(seat, index) : <div></div>
              )}
            </div>
          </div>
          <div class="row justify-content-md-center">
            <div class=" col-1 ">
              {seats.map((seat, index) =>
                index == 16 ? renderSeat(seat, index) : <div></div>
              )}
            </div>
            <div class=" col-1 ">
              {seats.map((seat, index) =>
                index == 17 ? renderSeat(seat, index) : <div></div>
              )}
            </div>
            <div class=" col-1 ">
              {seats.map((seat, index) =>
                index == 18 ? renderSeat(seat, index) : <div></div>
              )}
            </div>
            <div class=" col-1 ">
              {seats.map((seat, index) =>
                index == 19 ? renderSeat(seat, index) : <div></div>
              )}
            </div>
          </div>
        </main>
      </div>
      <div className="d-flex justify-content-center mt-4">
        <div className="d-flex flex-column align-items-center mx-3">
          <i className="bi bi-circle mx-2"></i>
          <span>Available: {availableCount}</span>
        </div>
        <div className="d-flex">
          Selected :
          {selectedSeat.map((number) => (
            <span key={number}>{number},</span>
          ))}
        </div>
      </div>
    <div class="modal fade" id="exampleModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLabel">{modalData.title}</h5>
          <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
        </div>
        <div class="modal-body">
          {modalData.body}
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
        </div>
      </div>
  </div>
</div>

     
  
</div>
</>
  );
}
export default ManageRide;




