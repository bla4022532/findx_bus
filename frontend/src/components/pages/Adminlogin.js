import React, { Fragment, useState } from "react";
import { useNavigate } from 'react-router-dom';
import backgroundImage from '../images/dark.jpg';

const Login = () => {
  const [email, setEmail] = useState('');
  const [password, setPassword] = useState('');
  const [error, setError] = useState('');
  const navigate = useNavigate();

  const onSubmitForm = async (e) => {
    e.preventDefault();
    if (!email || !password) {
      setError('All fields are required');
      return;
    }
    try {
      const response = await fetch('http://localhost:8080/login', {
        method: 'POST',
        headers: {
          'Content-Type': 'application/json'
        },
        body: JSON.stringify({ email, password })
      });

      const data = await response.json();
      if (response.ok) {
        navigate('/add');
      } else {
        setError(data.error); // Update to access the error message from the response
        console.log('WRONG EMAIL OR PASSWORD');
      }
      console.log(data);
    } catch (error) {
      console.error(error);
    }
  };

  return (
    <Fragment>
      <div className="login-container" style={{ backgroundImage: `url(${backgroundImage})`, backgroundSize: 'cover', backgroundPosition: 'center', height: '100vh' }}>
        <h1 className="mt-5 text-center text-white">Login</h1>
        <div className="container mt-5">
          <div className="row justify-content-center">
            <div className="col-md-4" style={{ border: "2px solid #ccc", borderRadius: "10px", padding: "20px" }}>
              <form onSubmit={onSubmitForm}>
                {error && <p className="text-white">{error}</p>}
                <div className="form-group">
                  <label>Email address</label>
                  <input
                    type="email"
                    className="form-control text-dark"
                    name="email"
                    value={email}
                    onChange={(e) => setEmail(e.target.value)}
                    style={{ color: 'dark' }}
                  />
                </div>
                <div className="form-group">
                  <label>Password</label>
                  <input
                    type="password"
                    className="form-control"
                    name="password"
                    value={password}
                    onChange={(e) => setPassword(e.target.value)}
                    style={{ color: 'dark' }}
                  />
                </div>
                <button type="submit" className="btn btn-success btn-block">Login</button>
              </form>
            </div>
          </div>
        </div>
      </div>
    </Fragment>
  );
};

export default Login;
