import React  from 'react';
// import './App.css';

import Home from './components/pages/Home';
import About from './components/pages/About';
import BookedTicket from './components/pages/BookedTicket';

import Contact from './components/pages/Contact';
import { BrowserRouter as Router,Route,Routes } from 'react-router-dom';

import Adminlogin from './components/pages/Adminlogin';
import Viewseat from './components/pages/Viewseat';
import Seat from './components/pages/Seat';
import Userdetails from './components/pages/Userdetail';
import DeleteUser from "./components/pages/Adminpage/DeleteUser";
import AddUser from './components/pages/Adminpage/AddUser';
import CreateRide from './components/pages/Adminpage/CreateRide';
import ManageRide from './components/pages/Adminpage/ManageRide';
import ManageRides from './components/pages/Adminpage/managerides';


import Add from './components/pages/Adminpage/Add';
import BookedDetails from './components/pages/BookedDetails';









function App() {
  return (
      <>
      <Router>
       
        <div className="pages">
          <Routes>
            <Route path="/" element={<Home />} />
            <Route path="/about" element={<About />} />
            <Route path="/bookticket" element={<BookedTicket/>} />
            <Route path="/contact" element={<Contact/>} />
            <Route path="/admin" element={<Adminlogin/>} />
            <Route path="/viewseat/:from/:to/:date" element={<Viewseat/>} />
            <Route path="/seats/:busID" element={<Seat/>} />
            <Route path="/userdetail/:busID/:seats" element={<Userdetails/>} />
            <Route path="/admin" element={<Add/>} /> 
            {/* <Route path="/deluser" element={<DeleteUser/>} /> */}
            {/* <Route path="/createride"  element={<CreateRide/>} /> */}
            <Route path="add" element={<Add/>}>
              <Route path="deluser" element={<DeleteUser />} />
              <Route path="adduser" element={<AddUser />} />
              <Route path="createride" element={<CreateRide />} />
              <Route path="managerides" element={<ManageRides />} />
            </Route>
            {/* <Route path="/adduser" element={<AddUser/>} /> */}
            <Route path="/manageride/:busID" element={<ManageRide/>} /> 
            {/* <Route path="/deluser" exact component={DeleteUser} /> */}
            {/* <Route path="/adduser" component={AddUser} /> */}
            {/* <Route path="/createride" component={CreateRide} /> */}
            <Route path="/manageride" component={ManageRide} />
            {/* <Route path="/managerides" element={<ManageRides/>} /> */}
            <Route path="/bookeddetails/:busID/:name/:email/:cid/:seats" element={<BookedDetails/>} />
          </Routes>
        </div>
       
      </Router>
      {/* <Seat></Seat> */}
  </>
    
  )
}

export default App;
