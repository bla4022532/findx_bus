const express = require('express');
const multer = require('multer');
const { logger } = require('./middlewares/logger');
const Admin_router = require('./routes/Admin/Admin_routes');
const User_router = require('./routes/Users/Users_route');
const cors = require('cors')
const app = express()

//@des middlewares
app.use(express.json());
app.use(express.urlencoded({extended:true}));
app.use(cors())
app.use(logger);
app.use(Admin_router)
app.use(User_router)
const PORT  = process.env.PORT || 8080



app.listen(PORT,()=>{console.log('server running on port 8080')});