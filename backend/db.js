const Pool = require("pg").Pool;

const pool = new Pool({
  host: "localhost",
  user: "nima_wangdi_user",
  password: "root",
  port: 5432,
  database: "revman"
});

module.exports = pool;