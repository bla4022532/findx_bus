const express = require('express');
const { createBus, postCreateBus, getBus,deleteBus} = require('../../controllers/Admin/createBus/createBus');
const { getMangeRides, PostMangeRides, postFeedback,getfeedback,deletefeedback} = require('../../controllers/Admin/manage-ride/manageRides');
const { getUser, postDeleteUser, postAddUser } = require('../../controllers/Admin/update-users/update-users');
const { postAdminAuth, register, login } = require('../../controllers/Admin/auth/admin_auth');


const Admin_router = express.Router();
//@des create bus
Admin_router.route('/create-bus')
.get(createBus)
.post(postCreateBus)
.delete(deleteBus)
Admin_router.route('/feedback')
.post(postFeedback)
.get(getfeedback)
.delete(deletefeedback)
Admin_router.route('/bus/:busID')
.get(getBus)
//@des manage rides
Admin_router.route('/manage-ride/:busID')
.get(getMangeRides)
.post(PostMangeRides)

//@des admin auth
Admin_router.route('/register')
.get(getMangeRides)
.post(register)

Admin_router.route('/login')
.post(login)
//@des updates user
Admin_router.route('/deluser')
.get(getUser)
.delete(postDeleteUser)
.post(postAddUser)

module.exports = Admin_router