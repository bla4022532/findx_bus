const express = require('express');
const { postUserAuth } = require('../../controllers/Users/userAuth/userAuth');
const { postBookedBus } = require('../../controllers/Users/bookedBus/seat-book');
const { postsearchBuses } = require('../../controllers/Users/bookedBus/bus_service');



const User_router = express.Router();
//@des create bus
User_router.route('/user-auth')

.post(postUserAuth)

User_router.route('/book-seat')
.post(postBookedBus)

User_router.route('/search-Bus')
.post(postsearchBuses)

module.exports = User_router