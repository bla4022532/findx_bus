const express = require("express");
const router = express.Router();
const pool = require("../db");
const bcrypt = require("bcrypt");
const validInfo = require("../middlewares/validinfo");
const authorization = require("../middlewares/authorization");
const revmanGenerator = require("../utils/revmanGenerator");




//  table 


pool.query(
  `CREATE TABLE IF NOT EXISTS users (
    user_id uuid DEFAULT uuid_generate_v4(),
    user_name VARCHAR(255) NOT NULL,
    user_email VARCHAR(255) NOT NULL UNIQUE,
    user_password VARCHAR(255) NOT NULL,
    PRIMARY KEY(user_id)
);`,
  (err, res) => {
    if (err) {
      console.log(err);
    } else {
      console.log('images table initialized');
    }
  }
);

// register



router.post("/register",validInfo, async (req, res) => {
    const { email, name, password } = req.body;
  
    try {
      const user = await pool.query("SELECT * FROM users WHERE user_email = $1", [
        email
      ]);
  
      if (user.rows.length > 0) {
        return res.status(401).json("User already exist!");
      }
  
      const salt = await bcrypt.genSalt(10);
      const bcryptPassword = await bcrypt.hash(password, salt);
  
      let newUser = await pool.query(
        "INSERT INTO users (user_name, user_email, user_password) VALUES ($1, $2, $3) RETURNING *",
        [name, email, bcryptPassword]
      );
  
      const revmanToken = revmanGenerator(newUser.rows[0].user_id);
  
      return res.json({ revmanToken });

    } catch (err) {
      console.error(err.message);
      res.status(500).send("Server error");
    }
  });

  // login
router.post("/login",validInfo, async (req, res) => {
    const { email, password } = req.body;
    try {
      const user = await pool.query("SELECT * FROM users WHERE user_email = $1", [
          email
        ]);
    
        if (user.rows.length === 0) {
          return res.status(401).json("Email not found");
        }

        const validPassword = await bcrypt.compare(
          password,
          user.rows[0].user_password
        );
    
        if (!validPassword) {
          return res.status(401).json("Password Incorrect");
        }
        const revmanToken = revmanGenerator(user.rows[0].user_id);
        return res.json({ revmanToken });

    } catch (err) {
        console.error(err.message);
        res.status(500).send("Server error");
      }
    });

    router.get("/verify", authorization, (req, res) => {
        try {
          res.json(true);

        } catch (err) {
          console.error(err.message);
          res.status(500).send("Server error");
        }
      });

module.exports = router;