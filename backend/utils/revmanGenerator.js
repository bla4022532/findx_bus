const revman = require("jsonwebtoken");
require("dotenv").config();

function revmanGenerator(user_id) {
  const payload = {
    user: {
      id: user_id
    }
  };
  return revman.sign(payload, process.env.revmanSecret, { expiresIn: "1h" });
}

  module.exports = revmanGenerator;