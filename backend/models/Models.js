const { DataTypes } = require('sequelize');
const sequelize = require('../database/pg_connection');
const Bus = sequelize.define("createBus", {
    id: {
        type: DataTypes.UUID,
        defaultValue: DataTypes.UUIDV4,
        primaryKey: true,

    },
    busName: {
        type: DataTypes.STRING,

        
    },
    DepatureTime: {
        type: DataTypes.TIME,
        defaultValue: DataTypes.NOW,

    },
   
    price_per_seat: {
        type: DataTypes.INTEGER,
        
    },
    date: {
        type: DataTypes.DATEONLY,
        
    },
    
    from: {
        type: DataTypes.STRING,
    
        
    }
    ,
    to:{
        type:DataTypes.STRING,
    
    },
    totalseat:{
        type: DataTypes.INTEGER,
        defaultValue:19, 
          
    }


},
);
const feedback = sequelize.define("feedbacks", {
    id: {
        type: DataTypes.UUID,
        defaultValue: DataTypes.UUIDV4,
        primaryKey: true,

    },
    username: {
        type: DataTypes.STRING,

        
    },
    phonenumber: {
        type: DataTypes.INTEGER,

    },
   
    email: {
        type: DataTypes.STRING,
        
    },
    feedback: {
        type: DataTypes.STRING,
        
    },

},
);

//@des bulk creat seat


//@description Admin registration
const Admin = sequelize.define('Admin', {
    id: {
        type: DataTypes.UUID,
        defaultValue: DataTypes.UUIDV4,
        primaryKey: true,

    },

    email: {
        type: DataTypes.STRING,
        allowNull: false

    },
    password: {
        type: DataTypes.STRING,
    }

}, );

const User= sequelize.define('user', {
    id: {
        type: DataTypes.UUID,
        defaultValue: DataTypes.UUIDV4,
        primaryKey: true,

    },

    email: {
        type: DataTypes.STRING,
        allowNull: false

    },
    password: {
        type: DataTypes.STRING,
    },

    cid:{
        type:DataTypes.INTEGER,
        allowNull:false
    },
    is_booked:{
        type:DataTypes.BOOLEAN,
        defaultValue: false
    }

}, );




//@des association 

// one to many 
// single admin can create many bus-session 
Admin.hasMany(Bus,{as:'addBus'});
User.belongsToMany(Bus,{through:'userBus',as:'bus'})
Bus.belongsToMany(User,{through:'userBus',as:'user'})
Bus.belongsTo(Admin)

sequelize.sync({ alter: true});
try {
    sequelize.authenticate();
    console.log('connection established');

} catch (e) {
    console.error("unable to connect")

}
console.log("The table for the User model was just (re)created!");

module.exports = {
    Bus, 
    Admin,User,feedback,
}