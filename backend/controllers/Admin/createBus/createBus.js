const { Bus, Admin } = require("../../../models/Models");
const pool = require("../../../database/pg_pool")

exports.createBus = async (req, res, next) => {
  try {
    const bus = await Bus.findAll();
    res.send(bus)
    
  } catch (e) {
    console.log(e)
  }
};

exports.postCreateBus = async (req, res, next) => {
  try {
    const {
      busName,
      DepatureTime,
      price_per_seat,
      totalseat,
      date,
      from,
      to
    } = req.body;

    const admin = await Admin.findOne();
    const Createbus = await admin.createAddBus({
      busName,
      DepatureTime,
      price_per_seat,
      totalseat,
      date,
      from,
      to
    });

    // @des admin createBus
    res.status(200).json({ message: 'Bus ride created successfully', bus: Createbus });
  } catch (error) {
    console.error(error);
    res.status(500).json({ message: 'An error occurred while creating the bus ride' });
  }
};

exports.getBus = async(req, res, next) => {
  const {busID} = req.params;
  console.log("bus id !!!", busID);
  try{
      const busTb = await pool.query("SELECT * FROM \"createBuses\" WHERE id = $1", [busID]);

      if (busTb.rows.length === 0){
          return res.status(404).send("Not found!");
      }

      console.log("bus found", busTb.rows[0]);
      res.status(200).json(busTb.rows[0]);
  }
  catch(err){
      console.log(err.message);
      res.status(500).send(err.message);
  }
};

exports.deleteBus = async (req, res, next) => {
  const { busID } = req.body;

  console.log("DELETEING BUS ", busID)
  try{
      await pool.query("BEGIN");

      try{
        await pool.query("DELETE FROM passenger_details WHERE busid = $1", [busID]);

        await pool.query("DELETE FROM \"createBuses\" WHERE id = $1", [busID]);

        await pool.query("COMMIT"); 
        return res.status(200).send("bus deleted");
      }
      catch(err){
        await pool.query("ROLLBACK");
        console.log(err.message);
        return res.status(500).send(err.message);
      }
  }
  catch(err){
      console.log(err.message);
      res.status(500).send("server error", err.message);
  }
};


