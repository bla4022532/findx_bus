const pool = require("../../../database/pg_pool")
const nodemailer = require("nodemailer")

async function sendEmail(to, subject, text) {
    let testAccount = await nodemailer.createTestAccount();
  
    let transporter = nodemailer.createTransport({
      host: "smtp.zoho.com",
      secure: true,
      port: 465,
      auth: {
        user: "busbooking@zohomail.com",
        pass: "@Dawa_jerry123",
      },
    });
  
    await transporter.sendMail({
      from: "busbooking@zohomail.com",
      to: to,
      subject: subject,
      text: text
    });
}

exports.getMangeRides = async (req,res,next)=>{
    const {busID} = req.params;

    try{
        const passengerTb = await pool.query("SELECT * FROM passenger_details WHERE busid = $1", [busID]);

        if (passengerTb.rows.length === 0){
            return res.status(404).send("Not found!");
        }

        let seats = [];
        passengerTb.rows.forEach(row => {
            row.seats.forEach(seat => {
                if (!seats.includes(seat)) seats.push(seat);
            })
        })
        
        console.log(seats)
        res.status(200).json({seats, passengerDetails: passengerTb.rows});
    }
    catch(err){
        console.log(err.message);
        res.status(500).send(err.message);
    }
}

exports.PostMangeRides = async (req,res,next)=>{
    const {busID} = req.params;
    const {name, email, cid, seats} = req.body;


    try{
        const busTb = await pool.query("SELECT * FROM \"createBuses\" WHERE id = $1", [busID]);
        const bus = busTb.rows[0];

        await pool.query("BEGIN");
        try{
            await pool.query(
                "INSERT INTO passenger_details (name, email, cid, busID, seats)\
                VALUES ($1, $2, $3, $4, $5::bigint[])",
                [name, email, cid, busID, seats.split(',').map(seat => parseInt(seat))]
            );
            let departureTime = bus.DepatureTime;
            if (!departureTime.endsWith("AM") && !departureTime.endsWith("PM")) {
              const hours = parseInt(departureTime.split(":")[0]);
              const suffix = hours >= 12 ? "PM" : "AM";
              const hour12 = hours % 12 || 12;
              const minutes = departureTime.split(":")[1];
              departureTime = `${hour12}:${minutes} ${suffix}`;
            }
      
            const body = 
`Passenger name: ${name}
CID: ${cid}
Bus name: ${bus.busName}
Departure time: ${departureTime}
From: ${bus.from}
To: ${bus.to}
Date: ${new Date(bus.date).toDateString()}
Price: ${bus.price_per_seat}
Seats: ${seats}`

            await sendEmail(email, "Your bus reservation details", body)

            await pool.query("COMMIT");
            return res.status(200).send("success!");
        }
        catch(err){
            await pool.query("ROLLBACK");
            return res.status(500).send(err.message);
        }
    }
    catch(err){
        console.log(err.message);
        res.status(500).send(err.message);
    }
}

exports.postFeedback = async(req, res, next) => {
    try {
      const {
        username,
        phonenumber,
        email,
        feedback,
      }=req.body;
  
      await pool.query(
        "INSERT INTO contacts(username, phonenumber, email, feedback)\
        VALUES ($1, $2, $3, $4)",
        [username, phonenumber, email, feedback]
      );
  
      // @des admin createBus
      res.status(200).json({ message: 'Posted successfully' });
    } catch (error) {
      console.error(error);
      res.status(500).json({ message: 'An error occurred while creating the feedback' });
    }
};

exports.getfeedback = async (req,res,next)=>{
    try{
        const passengerTb = await pool.query("SELECT * FROM contacts");

        res.status(200).json(passengerTb.rows);
    }
    catch(err){
        console.log(err.message);
        res.status(500).send(err.message);
    }
}

exports.deletefeedback = async (req, res, next) => {
    const {id} = req.body;
  
    try {
      await pool.query('DELETE FROM contacts WHERE id = $1', [id]);
      res.status(200).json({ message: 'Feedback deleted successfully' });
    } catch (err) {
      console.log(err.message);
      res.status(500).send(err.message);
    }
  };