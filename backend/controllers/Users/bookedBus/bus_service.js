const { Admin, Bus, User } = require("../../../models/Models");

exports.postsearchBuses = async (req, res, next) => {
  try {
    const { from, to,date  } = req.body;

    const buses = await Bus.findAll({
      where: {
        from: from,
        to: to,
       date:date
        
      },
      attributes:['from','to']
    });

    return res.json(buses);
  } catch (error) {
    console.log(error);
    res.status(500).json({ error: 'An error occurred while searching buses' });
  }
};
 