const Pool = require("pg").Pool;
const dotenv = require("dotenv");
dotenv.config();

const config = {
  connectionString: process.env.URL,
};

const devConfig = {
  host: "localhost",
  user: "postgres",
  password: "nima123",
  port: 5432,
  database: "postgres",
};

const pool = new Pool(devConfig);
module.exports = pool;
